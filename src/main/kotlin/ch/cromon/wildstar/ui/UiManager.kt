package ch.cromon.wildstar.ui

import ch.cromon.wildstar.ui.elements.*
import ch.cromon.wildstar.ui.messaging.WindowMessage
import ch.cromon.wildstar.ui.text.StaticText
import ch.cromon.wildstar.ui.utils.ClipStack
import ch.cromon.wildstar.ui.utils.TransformStack
import ch.cromon.wildstar.ui.views.ViewManager
import ch.cromon.wildstar.ui.views.files.LineDrawer
import glm.Java
import glm.mat4x4.Mat4
import glm.vec2.Vec2
import org.lwjgl.opengl.GL11.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component


@Component
    class UiManager {
    private lateinit var uiRoot: ItemsElement
    private val clipStack = ClipStack()
    private val transformStack = TransformStack({ globalTransformChanged(it) })
    val context = UiContext(clipStack, transformStack)

    @Autowired
    private lateinit var viewManager: ViewManager

    @Autowired
    private lateinit var debugLog: DebugLog

    fun updateFps(fps: Float) {
    }

    fun onInitialize() {
        StaticText.initialize()
        Quad.initialize()
        Line.initialize()
        MemoryControl.initialize()
        Image.initialize()
        LineDrawer.initialize()

        Quad.updateGlobalTransform(Mat4())
        StaticText.updateGlobalTransform(Mat4())
        Line.updateGlobalTransform(Mat4())
        LineDrawer.updateGlobalTransform(Mat4())

        uiRoot = ItemsElement()

        debugLog.onInitialize()

        viewManager.attach(uiRoot)
    }

    fun onResize(x: Int, y: Int) {
        val projection = Java.Glm.ortho(0.0f, x.toFloat(), y.toFloat(), 0.0f)
        StaticText.updateProjection(projection)
        Quad.updateProjection(projection)
        Line.updateProjection(projection)
        MemoryControl.updateProjection(projection)
        Image.updateProjection(projection)
        LineDrawer.updateProjection(projection)
        clipStack.windowHeight = y
        uiRoot.size = Vec2(x, y)
        debugLog.position = Vec2(0, y - 400)
        debugLog.size = Vec2(x, 400)
    }

    fun onFrame() {
        glDisable(GL_DEPTH_TEST)

        uiRoot.onFrame(context)
        //debugLog.onFrame(context)

        glEnable(GL_DEPTH_TEST)
    }

    fun handleMessage(message: WindowMessage) {
        uiRoot.handleMessage(message)
    }

    private fun globalTransformChanged(matrix: Mat4) {
        Quad.updateGlobalTransform(matrix)
        StaticText.updateGlobalTransform(matrix)
        Line.updateGlobalTransform(matrix)
        LineDrawer.updateGlobalTransform(matrix)
    }
}