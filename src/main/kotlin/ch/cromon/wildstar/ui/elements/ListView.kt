package ch.cromon.wildstar.ui.elements

import ch.cromon.wildstar.gx.BlendMode
import ch.cromon.wildstar.ui.UiContext
import ch.cromon.wildstar.ui.text.StaticText
import glm.vec2.Vec2
import java.awt.Color
import java.awt.Font

private class ListViewItem(val item: Any, val text: String = item.toString(), var drawer: StaticText? = null)

class ListView : Element() {
    companion object {
        private val FONT = Font("Arial", Font.BOLD, 16)
    }

    private lateinit var background: Quad
    private lateinit var border: Border
    private val itemList = ArrayList<ListViewItem>()

    private var visibleItemsStart = 0
    private var visibleItemsEnd = 0
    private var scrollOffset = 0.0f

    private var clipStart = Vec2()
    private var clipEnd = Vec2()

//    private lateinit var verticalScrollBar: Quad
//    private lateinit var horizontalScrollBar: Quad

    fun addItem(item: Any) {
        itemList.add(ListViewItem(item))
        onItemsChanged()
    }

    override fun onInitialize() {
        super.onInitialize()

        background = Quad()
        border = Border()

        border.color = Color.WHITE
        background.blendMode = BlendMode.ALPHA
        background.color = Color(0xDD222222.toInt(), true)
    }

    override fun onFrame(context: UiContext) {
        background.onFrame()
        border.onFrame()

        context.clipStack.push(clipStart, clipEnd)

        var currentPosition = -scrollOffset + 5
        for(i in visibleItemsStart..visibleItemsEnd) {
            val item = itemList[i]
            val drawer = item.drawer ?: StaticText(FONT)
            if(item.drawer == null) {
                drawer.text = item.text
                item.drawer = drawer
            }

            drawer.position = Vec2(calculatedPosition.x + 5, calculatedPosition.y + currentPosition)
            drawer.onFrame()
            currentPosition += 20
        }

        context.clipStack.pop()
    }

    override fun positionChanged() {
        super.positionChanged()
        positionSizeChanged()
    }

    override fun sizeChanged() {
        super.sizeChanged()
        positionSizeChanged()
    }

    private fun positionSizeChanged() {
        background.position = calculatedPosition
        background.size = calculatedSize

        border.topLeft = calculatedPosition
        border.bottomRight = calculatedPosition + calculatedSize

        clipStart = calculatedPosition + Vec2(5, 5)
        clipEnd = calculatedPosition + calculatedSize - Vec2(5, 5)
    }

    private fun onItemsChanged() {
        var index = 0
        var currentPos = -scrollOffset
        visibleItemsStart = 0
        visibleItemsEnd = 0

        var hasStart = false
        itemList.forEach {
            if(currentPos >= -25 && !hasStart) {
                visibleItemsStart = index
                hasStart = true
            }

            if(currentPos < calculatedSize.y + 10) {
                visibleItemsEnd = index
            }

            ++index
            currentPos += 20
        }
    }
}