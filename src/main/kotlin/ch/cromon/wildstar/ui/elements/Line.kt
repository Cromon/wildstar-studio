package ch.cromon.wildstar.ui.elements

import ch.cromon.wildstar.gx.*
import ch.cromon.wildstar.utils.toVec4
import glm.mat4x4.Mat4
import glm.vec2.Vec2
import org.lwjgl.BufferUtils
import org.lwjgl.opengl.GL11.GL_CULL_FACE
import org.lwjgl.opengl.GL11.glDisable
import java.awt.Color
import kotlin.properties.Delegates


class Line {
    private var colorNative = Color.WHITE.toVec4()
    private var vertexBuffer = VertexBuffer()
    private var gradientOffset = 1.0f

    private var startPos by Delegates.observable(Vec2(), { _, _, _ -> placementDataChanged() })
    private var endPos by Delegates.observable(Vec2(), { _, _, _ -> placementDataChanged() })
    var color: Color by Delegates.observable(Color.WHITE, { _, _, _ -> colorChanged() })
    private var thickness by Delegates.observable(4.0f, { _, _, _ -> placementDataChanged() })

    fun onFrame() {
        glDisable(GL_CULL_FACE)
        mesh.vertexBuffer = vertexBuffer
        mesh.program.setVec4(uniformColor, colorNative)
        mesh.program.setFloat(uniformGradientOffset, gradientOffset)
        mesh.render()
    }

    private fun placementDataChanged() {
        val ySizeWithGradient = (thickness / 2.0f) + 2 // 4 pixel of fading
        gradientOffset = (thickness / 2.0f) / ySizeWithGradient
        val topLeft = Vec2(startPos.x, startPos.y + ySizeWithGradient)
        val topRight = Vec2(endPos.x, endPos.y + ySizeWithGradient)
        val bottomLeft = Vec2(startPos.x, startPos.y - ySizeWithGradient)
        val bottomRight = Vec2(endPos.x, endPos.y - ySizeWithGradient)

        val bufferData = floatArrayOf(
                bottomLeft.x, bottomLeft.y, 0.0f, 1.0f,
                bottomRight.x, bottomRight.y, 1.0f, 1.0f,
                topRight.x, topRight.y, 1.0f, -1.0f,
                topLeft.x, topLeft.y, 0.0f, -1.0f
        )
        val dataBuffer = BufferUtils.createFloatBuffer(bufferData.size)
        dataBuffer.put(bufferData)
        dataBuffer.flip()

        vertexBuffer.data(dataBuffer)
    }

    private fun colorChanged() {
        colorNative = color.toVec4()
    }

    companion object {
        private lateinit var mesh: Mesh
        private var uniformProjection = -1
        private var uniformColor = -1
        private var uniformGlobalTransform = -1
        private var uniformGradientOffset = -1

        fun updateProjection(matrix: Mat4) {
            mesh.program.setMatrix(uniformProjection, matrix)
        }

        fun updateGlobalTransform(matrix: Mat4) {
            mesh.program.setMatrix(uniformGlobalTransform, matrix)
        }

        fun initialize() {
            mesh = Mesh()

            val program = Program()
            program.compileVertexShaderFromResource(Shader.LINE.vertexShader)
            program.compileFragmentShaderFromResource(Shader.LINE.fragmentShader)
            program.link()

            uniformProjection = program.getUniform("matProjection")
            uniformColor = program.getUniform("lineColor")
            uniformGlobalTransform = program.getUniform("globalTransform")
            uniformGradientOffset = program.getUniform("gradientOffset")

            mesh.program = program
            mesh.blendMode = BlendMode.ALPHA

            mesh.addElement(VertexElement("position", 0, 2))
            mesh.addElement(VertexElement("texCoord", 0, 2))

            mesh.finalize()

            val ib = IndexBuffer(IndexType.UINT8)
            val indexData = byteArrayOf(0, 1, 2, 0, 2, 3)
            val ibData = BufferUtils.createByteBuffer(indexData.size)
            ibData.put(indexData).flip()
            ib.data(ibData)

            mesh.indexBuffer = ib
            mesh.indexCount = 6
        }
    }
}