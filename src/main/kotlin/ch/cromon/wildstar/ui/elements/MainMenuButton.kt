package ch.cromon.wildstar.ui.elements

import ch.cromon.wildstar.io.ImageSource
import ch.cromon.wildstar.ui.UiContext
import ch.cromon.wildstar.ui.messaging.MouseClickMessage
import ch.cromon.wildstar.ui.messaging.MouseMoveMessage
import ch.cromon.wildstar.ui.messaging.WindowMessage
import ch.cromon.wildstar.utils.Event
import ch.cromon.wildstar.utils.EventHandler
import ch.cromon.wildstar.utils.pointInQuad
import glm.vec2.Vec2
import java.awt.Color
import kotlin.properties.Delegates


class MainMenuButton : ItemsElement() {
    private lateinit var backgroundHover: Quad
    private lateinit var backgroundClick: Quad
    private lateinit var border: Border
    private lateinit var caption: Label
    private lateinit var image: Image

    private var bottomRight = Vec2()

    private var isHovered = false
    private var isClicked = false
    private val clickEvent = EventHandler<MainMenuButton>()

    var imageSource by Delegates.observable(null as ImageSource?, { _, _, _ -> imageSourceChanged() })
    var text by Delegates.observable("", { _, _, _ -> textChanged() })

    val onClick = Event(clickEvent)

    override fun handleMessage(message: WindowMessage) {
        if(message is MouseClickMessage) {
            if(!disabled && isClicked && !message.isHandled && !message.pressed && pointInQuad(message.position, calculatedPosition, bottomRight)) {
                clickEvent(this)
            }
            isClicked = !message.isHandled && message.pressed && pointInQuad(message.position, calculatedPosition, bottomRight)
            if(isClicked) {
                message.isHandled = true
            }
        } else if (message is MouseMoveMessage) {
            isHovered = !message.isHandled && pointInQuad(message.position, calculatedPosition, bottomRight)
            if(isHovered) {
                message.isHandled = true
            }
        }
    }

    override fun onFrame(context: UiContext) {
        if (isClicked) {
            backgroundClick.onFrame()
        } else if (isHovered) {
            backgroundHover.onFrame()
        }

        super.onFrame(context)

        border.onFrame()
    }

    override fun positionChanged() {
        super.positionChanged()

        sizePositionChanged()
    }

    override fun sizeChanged() {
        super.sizeChanged()

        sizePositionChanged()
    }

    override fun onInitialize() {
        super.onInitialize()

        backgroundHover = Quad()
        backgroundClick = Quad()
        border = Border()

        caption = Label()
        image = Image()

        appendChild(caption)
        appendChild(image)

        image.position = Vec2(10, 5)
        image.size = Vec2(150, 150)

        caption.fontSize = 24

        backgroundClick.color = Color(0x444444)
        backgroundHover.color = Color(0x555555)
    }

    private fun sizePositionChanged() {
        border.topLeft = calculatedPosition
        border.bottomRight = calculatedPosition + calculatedSize

        image.position = calculatedPosition + Vec2(10, 10)

        val textWidth = caption.textWidth
        caption.position = Vec2((calculatedSize.x - textWidth) / 2.0f, calculatedSize.y - 50)

        bottomRight = calculatedPosition + calculatedSize

        backgroundClick.position = calculatedPosition
        backgroundClick.size = calculatedSize

        backgroundHover.position = calculatedPosition
        backgroundHover.size = calculatedSize
    }

    private fun imageSourceChanged() {
        image.imageSource = imageSource
    }

    private fun textChanged() {
        caption.text = text
        sizePositionChanged()
    }
}