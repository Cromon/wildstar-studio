package ch.cromon.wildstar.ui.elements

import ch.cromon.wildstar.ui.UiContext
import ch.cromon.wildstar.ui.messaging.WindowMessage
import ch.cromon.wildstar.ui.utils.Alignment
import ch.cromon.wildstar.ui.utils.AlignmentManager
import glm.vec2.Vec2
import kotlin.properties.Delegates


abstract class Element {
    private var skipAlignmentCalculation = false

    internal var calculatedPosition: Vec2 = Vec2()
    internal var calculatedSize: Vec2 = Vec2()

    internal var isInitialized = false

    var parent: ItemsElement? by Delegates.observable(null as ItemsElement?, { _, _, _ -> parentChanged() })
    var position: Vec2 by Delegates.observable(Vec2(), { _, _, _ -> positionChanged() })
    var size: Vec2 by Delegates.observable(Vec2(), { _, _, _ -> sizeChanged() })
    var horizontalAlignment: Alignment by Delegates.observable(Alignment.START, { _, old, new -> alignmentChanged(old, new) })
    var verticalAlignment: Alignment by Delegates.observable(Alignment.START, { _, old, new -> alignmentChanged(old, new) })

    var visible = true
    var disabled = false

    abstract fun onFrame(context: UiContext)

    open fun handleMessage(message: WindowMessage) {

    }

    open fun onInitialize() {
        isInitialized = true
    }

    internal open fun onParentResize() {
        positionChanged()
        sizeChanged()
    }

    fun removeFromParent() {
        parent?.removeChild(this)
        parentChanged()
    }

    internal open fun positionChanged() {
        if(skipAlignmentCalculation) {
            return
        }

        val result = AlignmentManager.calculate(parent, this, horizontalAlignment, verticalAlignment)
        skipAlignmentCalculation = true
        this.calculatedPosition = result.position
        this.calculatedSize = result.size
        skipAlignmentCalculation = false
    }

    internal open fun sizeChanged() {
        if(skipAlignmentCalculation) {
            return
        }

        val result = AlignmentManager.calculate(parent, this, horizontalAlignment, verticalAlignment)
        skipAlignmentCalculation = true
        this.calculatedPosition = result.position
        this.calculatedSize = result.size
        skipAlignmentCalculation = false
    }

    private fun parentChanged() {
        val result = AlignmentManager.calculate(parent, this, horizontalAlignment, verticalAlignment)
        this.calculatedPosition = result.position
        this.calculatedSize = result.size
    }

    private fun alignmentChanged(old: Alignment, new: Alignment) {
        if(old == new) {
            return
        }

        val result = AlignmentManager.calculate(parent, this, horizontalAlignment, verticalAlignment)
        this.calculatedPosition = result.position
        this.calculatedSize = result.size
    }
}