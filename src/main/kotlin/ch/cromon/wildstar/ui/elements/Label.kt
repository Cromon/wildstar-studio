package ch.cromon.wildstar.ui.elements

import ch.cromon.wildstar.ui.UiContext
import ch.cromon.wildstar.ui.text.StaticText
import glm.vec2.Vec2
import java.awt.Font
import kotlin.properties.Delegates


class Label : Element() {
    private var textRender = StaticText(Font("Arial", Font.BOLD, 20))

    private var width = -1

    var fontSize by Delegates.observable(20, { _, _, _ -> fontSizeChanged() })
    var text by Delegates.observable("", { _, _, _ -> textChanged() })


    init {
        textRender.isMultiLine = false
    }

    val textHeight get() = textRender.textHeight
    val textWidth get() = textRender.textWidth

    override fun onFrame(context: UiContext) {
        var clipped = false
        if(width > 0) {
            context.clipStack.push(calculatedPosition, calculatedPosition + Vec2(width, textRender.textHeight))
            clipped = true
        }

        textRender.onFrame()

        if(clipped) {
            context.clipStack.pop()
        }
    }

    private fun fontSizeChanged() {
        textRender = StaticText(Font("Arial", Font.BOLD, fontSize))
        textRender.text = text
        textRender.position = calculatedPosition
    }

    private fun textChanged() {
        textRender.text = text
    }

    override fun positionChanged() {
        super.positionChanged()
        textRender.position = calculatedPosition
    }

}