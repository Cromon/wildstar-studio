package ch.cromon.wildstar.ui.elements

import ch.cromon.wildstar.gx.BlendMode
import ch.cromon.wildstar.ui.UiContext
import ch.cromon.wildstar.ui.text.StaticText
import glm.vec2.Vec2
import org.springframework.stereotype.Component
import java.awt.Color
import java.awt.Font

private class DebugLogMessage(val content: String, var drawer: StaticText? = null)

@Component
class DebugLog : Element() {
    companion object {
        private val FONT = Font("Arial", Font.BOLD, 16)
    }

    private val messageList = ArrayList<DebugLogMessage>()
    private lateinit var background: Quad
    private lateinit var border: Border

    private var clipStart = Vec2()
    private var clipEnd = Vec2()

    override fun onFrame(context: UiContext) {
        background.onFrame()
        border.onFrame()

        context.clipStack.push(clipStart, clipEnd)

        var currentPosition = calculatedPosition.y + calculatedSize.y - 20
        for(i in (messageList.size - 1) downTo 0) {
            val entry = messageList[i]
            val drawer = entry.drawer ?: StaticText(FONT)
            if(entry.drawer == null) {
                drawer.text = entry.content
                entry.drawer = drawer
            }

            drawer.position = Vec2(5, currentPosition)
            drawer.onFrame()
            currentPosition -= 20
            if(currentPosition < calculatedPosition.y - 30) {
                break
            }
        }

        context.clipStack.pop()
    }

    override fun onInitialize() {
        super.onInitialize()
        background = Quad()
        border = Border()

        border.drawBottom = false
        border.drawLeft = false
        border.drawRight = false

        background.color = Color(0xCC222222.toInt(), true)
        background.blendMode = BlendMode.ALPHA

        //DebugLogAppender.INSTANCE.callback = { onDebugMessage(it) }
    }

    override fun positionChanged() {
        super.positionChanged()
        positionSizeChanged()
    }

    override fun sizeChanged() {
        super.sizeChanged()
        positionSizeChanged()
    }

    /*private fun onDebugMessage(event: DebugLogEvent) {
        val message = event.message
        messageList.add(DebugLogMessage(message))
    }*/

    private fun positionSizeChanged() {
        background.position = calculatedPosition
        background.size = calculatedSize

        border.topLeft = calculatedPosition
        border.bottomRight = calculatedPosition + calculatedSize

        clipStart = calculatedPosition + Vec2(5, 5)
        clipEnd = calculatedPosition + calculatedSize - Vec2(5, 5)
    }
}