package ch.cromon.wildstar.ui.elements

import ch.cromon.wildstar.io.ImageSource
import ch.cromon.wildstar.io.archive.DirectoryEntry
import ch.cromon.wildstar.io.archive.FileEntry
import ch.cromon.wildstar.io.archive.FileSystemEntry
import ch.cromon.wildstar.ui.UiContext
import ch.cromon.wildstar.ui.messaging.MouseClickMessage
import ch.cromon.wildstar.ui.messaging.MouseMoveMessage
import ch.cromon.wildstar.ui.messaging.MouseWheelMessage
import ch.cromon.wildstar.ui.messaging.WindowMessage
import ch.cromon.wildstar.ui.text.StaticText
import ch.cromon.wildstar.utils.Event
import ch.cromon.wildstar.utils.EventHandler
import ch.cromon.wildstar.utils.pointInQuad
import glm.vec2.Vec2
import org.slf4j.LoggerFactory
import java.awt.Color
import java.awt.Font

class LazyTreeViewItem(private val entry: FileSystemEntry, private val fileClickedEvent: EventHandler<FileEntry>, private val clickCallback: () -> Unit) {
    companion object {
        private val LOG = LoggerFactory.getLogger(LazyTreeViewItem::class.java)
    }

    private var isLoaded = false
    private var isExpanded = false
    private var isHovered = false
    private var isClicked = false

    private lateinit var caption: StaticText
    private lateinit var image: Image
    private lateinit var hoverBackground: Quad
    private val children = ArrayList<LazyTreeViewItem>()

    private fun hasChildren() = entry.hasChildren()

    private fun expand() {
        if(!hasChildren()) {
            return
        }

        if(children.isEmpty()) {
            val dir = entry as DirectoryEntry
            children.addAll(dir.children.map { LazyTreeViewItem(it, fileClickedEvent, clickCallback) })
        }

        image.imageSource = LazyTreeView.getImageForEntry(entry, true)
        isExpanded = true
    }

    private fun collapse() {
        if(!hasChildren()) {
            return
        }

        image.imageSource = LazyTreeView.getImageForEntry(entry, false)
        isExpanded = false
    }

    fun onMessage(message: MouseMoveMessage, offsetX: Float, offsetY: Float, indentation: Float, maxWidth: Float): Float {
        if(message.isHandled || !isLoaded) {
            isHovered = false
            return offsetY
        }

        val contained = pointInQuad(message.position, Vec2(offsetX, offsetY), Vec2(offsetX + maxWidth, offsetY + 24))
        if(contained) {
            isHovered = true
            message.isHandled = true
        } else {
            isHovered = false
        }

        var realOffset = offsetY + 24
        if(isExpanded && hasChildren()) {
            children.forEach {
                realOffset = it.onMessage(message, offsetX, realOffset, indentation + 25, maxWidth)
            }
        }
        return realOffset
    }

    fun onMessage(message: MouseClickMessage, offsetX: Float, offsetY: Float, indentation: Float, maxWidth: Float): Float {
        if(message.isHandled || !isLoaded) {
            isClicked = false
            return offsetY
        }

        val contained = pointInQuad(message.position, Vec2(offsetX, offsetY), Vec2(offsetX + maxWidth, offsetY + 24))
        if(contained) {
            if(!message.pressed && isClicked) {
                onClick()
            } else if(message.pressed) {
                isClicked = true
            }
        } else {
            isClicked = false
        }

        var realOffset = offsetY + 24
        if(isExpanded && hasChildren()) {
            children.forEach {
                realOffset = it.onMessage(message, offsetX, realOffset, indentation + 25, maxWidth)
            }
        }
        return realOffset
    }

    fun onFrame(context: UiContext, offsetX: Float, offsetY: Float, indentation: Float, maxWidth: Float): Float {
        if(!isLoaded) {
            onLoad()
            isLoaded = true
        }

        if(isHovered) {
            hoverBackground.position = Vec2(offsetX, offsetY)
            hoverBackground.size = Vec2(maxWidth, 24)
            hoverBackground.onFrame()
        }

        image.position = Vec2(offsetX + indentation, offsetY - 2)
        image.onFrame(context)
        caption.position = Vec2(offsetX + indentation + 30, offsetY)
        caption.onFrame()

        var newOffset = offsetY + 24
        if(isExpanded) {
            for(child in children) {
                newOffset = child.onFrame(context, offsetX, newOffset, indentation + 25, maxWidth)
            }
        }

        return newOffset
    }

    fun getTotalSize(): Int {
        var offset = 24
        if(isExpanded) {
            offset += children.sumBy { it.getTotalSize() }
        }

        return offset
    }

    private fun onLoad() {
        caption = StaticText(Font("Arial", Font.BOLD, 20))
        caption.text = entry.name

        image = Image()
        image.size = Vec2(24, 24)
        image.imageSource = LazyTreeView.getImageForEntry(entry)

        hoverBackground = Quad()
        hoverBackground.color = Color(0x333333)
    }

    private fun onClick() {
        if(entry is FileEntry) {
            LOG.info("Clicked on file entry: {}", entry.getFullPath())
            fileClickedEvent(entry)
        } else {
            if(!isExpanded) {
                expand()
            } else {
                collapse()
            }
        }

        clickCallback()
    }
}

class LazyTreeView : Element() {
    internal companion object {
        private val directoryImage = ImageSource.fromImageResource("icon_folder_closed.png")
        private val directoryImageOpen = ImageSource.fromImageResource("icon_folder_open.png")
        private val otherImage = ImageSource.fromImageResource("icon_other.png")
        private val tableImage = ImageSource.fromImageResource("icon_table.png")
        private val terrainImage = ImageSource.fromImageResource("icon_terrain.png")
        private val textImage = ImageSource.fromImageResource("icon_text.png")
        private val textureImage = ImageSource.fromImageResource("icon_texture.png")
        private val modelImage = ImageSource.fromImageResource("icon_model.png")
        private val buildingImage = ImageSource.fromImageResource("icon_building.png")

        private val extensionMap = hashMapOf(
                ".tex" to textureImage,
                ".lua" to textImage,
                ".form" to textImage,
                ".xml" to textImage,
                ".txt" to textImage,
                ".tbl" to tableImage,
                ".m3" to modelImage,
                ".i3" to buildingImage,
                ".area" to terrainImage
        )

        fun getImageForEntry(entry: FileSystemEntry, expanded: Boolean = false): ImageSource {
            if(entry is DirectoryEntry) {
                return if(expanded) directoryImageOpen else directoryImage
            }

            val extIndex = entry.name.lastIndexOf('.')
            if(extIndex < 0) {
                return otherImage
            }

            val extension = entry.name.substring(extIndex).toLowerCase()
            return extensionMap.getOrDefault(extension, otherImage)
        }
    }

    private val children = ArrayList<LazyTreeViewItem>()
    private var topLeft = Vec2()
    private var bottomRight = Vec2()
    private val fileClickEvent = EventHandler<FileEntry>()

    private var hasScrollBar = false
    private var scrollOffset = 0.0f
    private var maxScrollOffset = 0.0f

    val fileClicked = Event(fileClickEvent)

    fun addAll(items: Collection<FileSystemEntry>) {
        children.addAll(items.map { LazyTreeViewItem(it, fileClickEvent, this::handleScrollUpdate) })
        handleScrollUpdate()
    }

    override fun onFrame(context: UiContext) {
        context.clipStack.push(topLeft, bottomRight)

        var curPosition = calculatedPosition.y - scrollOffset

        for(elem in children) {
            curPosition = elem.onFrame(context, calculatedPosition.x, curPosition, 0.0f, calculatedSize.x)
        }

        context.clipStack.pop()
    }

    override fun handleMessage(message: WindowMessage) {
        when (message) {
            is MouseMoveMessage -> {
                var offset = calculatedPosition.y - scrollOffset
                children.forEach { offset = it.onMessage(message, calculatedPosition.x, offset, 0.0f, calculatedSize.x) }
            }
            is MouseClickMessage -> {
                var offset = calculatedPosition.y - scrollOffset
                children.forEach { offset = it.onMessage(message, calculatedPosition.x, offset, 0.0f, calculatedSize.x) }
            }
            is MouseWheelMessage -> {
                if(pointInQuad(message.position, topLeft, bottomRight)) {
                    scrollOffset -= message.dy * 30
                    scrollOffset = Math.min(Math.max(0.0f, scrollOffset), maxScrollOffset)
                    handleMessage(MouseMoveMessage(message.position, message.context))
                }
            }
        }
    }

    override fun positionChanged() {
        super.positionChanged()
        sizePositionChanged()
    }

    override fun sizeChanged() {
        super.sizeChanged()
        sizePositionChanged()
    }

    private fun sizePositionChanged() {
        topLeft = calculatedPosition
        bottomRight = calculatedPosition + calculatedSize
        handleScrollUpdate()
    }

    private fun handleScrollUpdate() {
        if(calculatedSize.y < 24) {
            return
        }

        val totalSize = children.sumBy { it.getTotalSize() }
        if(totalSize > calculatedSize.y) {
            maxScrollOffset = totalSize - calculatedSize.y
            scrollOffset = Math.min(Math.max(0.0f, scrollOffset), maxScrollOffset)
            hasScrollBar = true
        } else {
            maxScrollOffset = 0.0f
            scrollOffset = 0.0f
            hasScrollBar = false
        }
    }
}