package ch.cromon.wildstar.ui.elements

import ch.cromon.wildstar.gx.*
import ch.cromon.wildstar.utils.format
import ch.cromon.wildstar.ui.UiContext
import ch.cromon.wildstar.ui.text.StaticText
import glm.mat4x4.Mat4
import glm.vec2.Vec2
import org.lwjgl.BufferUtils
import java.awt.Font
import java.util.*

private class Snapshot(val totalUsed : Long) {
    val timestamp = Date()
}

class MemoryControl : Element() {
    private val snapshots = ArrayList<Snapshot>()
    private val vertexBuffer = VertexBuffer()
    private val indexBuffer = IndexBuffer(IndexType.UINT16)
    private var lastUpdate = Date()
    private val minMemoryText = StaticText(Font("Arial", Font.BOLD, 14))
    private val maxMemoryText = StaticText(Font("Arial", Font.BOLD, 14))

    init {
        size = Vec2(200, 100)
        position = Vec2(100, 100)
    }

    override fun onFrame(context: UiContext) {
        val lastSnapshot = snapshots.map(Snapshot::timestamp).lastOrNull()
        if(lastSnapshot == null || (Date().time - lastSnapshot.time) > 50) {
            collectSnapshot()
        }

        drawSnapshots()

        if(mesh.indexCount > 1) {
            context.clipStack.push(position, position + size, true)
            mesh.vertexBuffer = vertexBuffer
            mesh.indexBuffer = indexBuffer
            mesh.render()
            context.clipStack.pop()
        }

        minMemoryText.onFrame()
        maxMemoryText.onFrame()
    }

    private fun drawSnapshots() {
        if(snapshots.size <= 1) {
            return
        }

        var minMemory = Long.MAX_VALUE
        var maxMemory = Long.MIN_VALUE

        snapshots.forEach {
            if(it.totalUsed < minMemory) minMemory = it.totalUsed
            if(it.totalUsed > maxMemory) maxMemory = it.totalUsed
        }

        if(minMemory == maxMemory) {
            minMemory -= 1
            maxMemory += 1
        }

        minMemoryText.text = formatMemory(minMemory)
        maxMemoryText.text = formatMemory(maxMemory)

        val factor = if(snapshots.size < 100) (Date().time - lastUpdate.time) / 50.0f else 0.0f
        val offset = factor * 2
        var curPos = -offset

        val ibData = BufferUtils.createShortBuffer(snapshots.size)
        val vbData = BufferUtils.createFloatBuffer(snapshots.size * 2)
        for((curIndex, snapshot) in snapshots.withIndex()) {
            val positionX = position.x + curPos
            curPos += 2
            val pct = (snapshot.totalUsed - minMemory).toFloat() / (maxMemory - minMemory).toFloat()
            val positionY = position.y + size.y - pct * size.y
            ibData.put(curIndex.toShort())
            vbData.put(positionX).put(positionY)
        }

        ibData.flip()
        vbData.flip()

        indexBuffer.data(ibData)
        vertexBuffer.data(vbData)
        mesh.indexCount = snapshots.size
    }

    private fun formatMemory(memory: Long): String {
        var value = memory.toFloat()
        var unit = "B"
        if(value > 1024) {
            value /= 1024.0f
            unit = "kB"
            if(value > 1024) {
                value /= 1024.0f
                unit = "mB"
                if(value > 1024) {
                    value /= 1024.0f
                    unit = "gB"
                }
            }
        }

        return "${value.format(2)} $unit"
    }

    private fun collectSnapshot() {
        val runtime = Runtime.getRuntime()!!
        val snapshot = Snapshot(runtime.totalMemory() - runtime.freeMemory())
        snapshots.add(snapshot)
        if(snapshots.size > 100) {
            snapshots.removeAt(0)
        }
        lastUpdate = Date()
    }

    override fun positionChanged() {
        maxMemoryText.position = Vec2(position.x, position.y - 20)
        minMemoryText.position = Vec2(position.x, position.y + size.y + 2)
    }

    companion object {
        private lateinit var mesh: Mesh
        private var uniformProjection = -1

        fun updateProjection(matrix: Mat4) {
            mesh.program.setMatrix(uniformProjection, matrix)
        }

        fun initialize() {
            mesh = Mesh()
            mesh.addElement(VertexElement("position", 0, 2))
            val program = Program()
            program.compileFragmentShaderFromResource(Shader.MEMORY_CONTROL.fragmentShader)
            program.compileVertexShaderFromResource(Shader.MEMORY_CONTROL.vertexShader)
            program.link()

            uniformProjection = program.getUniform("matProjection")

            mesh.program = program
            mesh.topology = Topology.LINE_STRIP
            mesh.finalize()
        }
    }
}