package ch.cromon.wildstar.ui.elements

import ch.cromon.wildstar.gx.*
import ch.cromon.wildstar.io.ImageSource
import ch.cromon.wildstar.ui.UiContext
import glm.mat4x4.Mat4
import org.lwjgl.BufferUtils
import kotlin.properties.Delegates


class Image : Element() {
    var imageSource by Delegates.observable(null as ImageSource?, { _, _, _ -> textureChanged = true })

    private var texture: Texture? = null
    private var textureChanged = false
    private var matTransform = Mat4()

    override fun onFrame(context: UiContext) {
        if(textureChanged) {
            updateTexture()
        }

        val tex = texture ?: return

        mesh.program.setMatrix(uniformTransform, matTransform)
        mesh.textureInput.add(uniformTexture, tex)
        mesh.render()
    }

    override fun positionChanged() {
        super.positionChanged()
        sizePositionChanged()
    }

    override fun sizeChanged() {
        super.sizeChanged()
        sizePositionChanged()
    }

    private fun updateTexture() {
        textureChanged = false
        if(imageSource == null) {
            texture = null
            return
        } else {
            val source = imageSource ?: return
            if(texture == null) {
                texture = Texture()
            }

            texture?.loadArgb(source)
        }
    }

    private fun sizePositionChanged() {
        matTransform = Mat4().translate(calculatedPosition.x, calculatedPosition.y, 0.0f).scale(calculatedSize.x, calculatedSize.y, 1.0f)
    }

    companion object {
        private lateinit var mesh: Mesh

        private var uniformProjection = -1
        private var uniformTransform = -1
        private var uniformTexture = -1

        fun updateProjection(mat: Mat4) {
            mesh.program.setMatrix(uniformProjection, mat)
        }

        fun initialize() {
            mesh = Mesh()

            val program = Program()
            program.compileFromResource(Shader.IMAGE)
            program.link()

            uniformProjection = program.getUniform("matProjection")
            uniformTexture = program.getUniform("imageTexture")
            uniformTransform = program.getUniform("matTransform")

            mesh.program = program
            mesh.indexCount = 4
            mesh.blendMode = BlendMode.ALPHA

            mesh.addElement(VertexElement("position", 0, 2))
            mesh.addElement(VertexElement("texCoord", 0, 2))
            mesh.finalize()

            mesh.topology = Topology.QUADS

            val ibData = BufferUtils.createByteBuffer(4)
            ibData.put(0).put(3).put(2).put(1).flip()

            mesh.indexBuffer = IndexBuffer(IndexType.UINT8)
            mesh.indexBuffer.data(ibData)

            val vbData = BufferUtils.createFloatBuffer(4 * 4)
            vbData.put(0.0f).put(0.0f).put(0.0f).put(0.0f)
                    .put(1.0f).put(0.0f).put(1.0f).put(0.0f)
                    .put(1.0f).put(1.0f).put(1.0f).put(1.0f)
                    .put(0.0f).put(1.0f).put(0.0f).put(1.0f)
                    .flip()

            mesh.vertexBuffer = VertexBuffer()
            mesh.vertexBuffer.data(vbData)
        }
    }
}