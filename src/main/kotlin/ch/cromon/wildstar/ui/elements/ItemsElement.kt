package ch.cromon.wildstar.ui.elements

import ch.cromon.wildstar.ui.UiContext
import ch.cromon.wildstar.ui.messaging.WindowMessage
import glm.mat4x4.Mat4
import glm.vec2.Vec2
import kotlin.properties.Delegates


open class ItemsElement : Element() {
    protected var childOffset: Vec2 by Delegates.observable(Vec2(), { _, _, _ -> positionChanged() })

    private var matrixTransform = Mat4()
    private var children = ArrayList<Element>()
    private var messageRelativePos = Vec2()
    private val childrenToRemove = ArrayList<Element>()
    private val childrenToAdd = ArrayList<Element>()
    private var isLooping = false

    internal fun getChildOffset(): Vec2 = childOffset

    fun appendChild(child: Element) {
        if (children.contains(child)) {
            return
        }

        child.parent = this
        if(!isLooping) {
            children.add(child)
        } else {
            childrenToAdd.add(child)
        }

        if (!child.isInitialized) {
            child.onInitialize()
        }

        child.onParentResize()
    }

    fun removeChild(child: Element) {
        if (!children.contains(child)) {
            return
        }

        child.parent = null
        if (!isLooping) {
            children.remove(child)
        } else {
            childrenToRemove.add(child)
        }
    }

    override fun onFrame(context: UiContext) {
        context.transformStack.pushMatrix(matrixTransform)

        applyToChildren({
            it.onFrame(context)
        })

        context.transformStack.popMatrix()
    }

    override fun handleMessage(message: WindowMessage) {
        super.handleMessage(message)

        val transformedMessage = message.forwardMessage(messageRelativePos)
        applyToChildren({ it.handleMessage(transformedMessage) }, true)

        message.isHandled = transformedMessage.isHandled
    }

    override fun positionChanged() {
        super.positionChanged()
        matrixTransform = Mat4().translate(calculatedPosition.x + childOffset.x, calculatedPosition.y + childOffset.y, 0.0f)
        messageRelativePos = calculatedPosition + childOffset
        applyToChildren({ it.onParentResize() })
    }

    override fun sizeChanged() {
        super.sizeChanged()
        matrixTransform = Mat4().translate(calculatedPosition.x + childOffset.x, calculatedPosition.y + childOffset.y, 0.0f)
        messageRelativePos = calculatedPosition + childOffset

        applyToChildren({ it.onParentResize() })

    }

    override fun onInitialize() {
        super.onInitialize()
        applyToChildren({
            if (!it.isInitialized) {
                it.onInitialize()
            }
        })
    }

    private fun applyToChildren(callback: (Element) -> Unit, reversed: Boolean = false) {
        val childList = children
        isLooping = true
        if (!reversed) {
            childList.forEach { callback(it) }
        } else {
            childList.reversed().forEach { callback(it) }
        }
        isLooping = false

        if(childrenToRemove.size > 0 || childrenToAdd.size > 0) {
            val childrenCopy = ArrayList(children)
            childrenCopy.removeAll(childrenToRemove)
            childrenCopy.addAll(childrenToAdd)
            children = childrenCopy
            childrenToRemove.clear()
            childrenToAdd.clear()
        }
    }
}