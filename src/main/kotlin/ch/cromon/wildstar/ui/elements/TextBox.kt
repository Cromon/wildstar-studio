package ch.cromon.wildstar.ui.elements

import ch.cromon.wildstar.gx.BlendMode
import ch.cromon.wildstar.utils.pointInQuad
import ch.cromon.wildstar.ui.UiContext
import ch.cromon.wildstar.ui.messaging.KeyPressMessage
import ch.cromon.wildstar.ui.messaging.MouseButton
import ch.cromon.wildstar.ui.messaging.MouseClickMessage
import ch.cromon.wildstar.ui.messaging.WindowMessage
import ch.cromon.wildstar.ui.text.StaticText
import glm.vec2.Vec2
import org.lwjgl.glfw.GLFW.*
import java.awt.Color
import java.awt.Font
import java.awt.image.BufferedImage
import java.util.*
import javax.swing.JLabel
import kotlin.properties.Delegates

private class TextMeasurer(font: Font) {
    private val label = JLabel()
    private val fontMetrics = label.getFontMetrics(font)
    private val graphics = BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB).createGraphics()

    fun getTextLength(text: String): Float {
        return fontMetrics.getStringBounds(text, graphics).width.toFloat()
    }

    fun getCharWidth(chr: Char): Float {
        return fontMetrics.getStringBounds(String(charArrayOf(chr)), graphics).width.toFloat()
    }
}

open class TextBox : Element() {
    companion object {
        private val NON_PRINT_CHARACTER: IntArray = intArrayOf(
                1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12,
                13, 14, 15, 16, 17, 18, 19, 20, 21, 22,
                23, 24, 25, 26, 27, 28, 29, 30, 31
        )
    }

    protected var originalText = ""
    private var text = ""
    private var visibleText by Delegates.observable("", { _, _, _ -> visibleTextChanged() })
    private var visibleTextOffset by Delegates.observable(5.0f, { _, _, _ -> visibleTextOffsetChanged() })

    private var caretOffset = 0
    private var visibleTextStart = 0
    private var lastCaretUpdate = Date()

    private var font = Font("Arial", Font.BOLD, 18)
    private var textMeasurer = TextMeasurer(font)

    private var hasFocus = false
    private var sizeOverride = false
    private var caretVisible = false

    private var clipStart = Vec2()
    private var clipEnd = Vec2()
    private var bottomRight = Vec2(0, 0)

    private val border = Border()
    private val caretQuad = Quad()
    private val background = Quad()
    private var textDraw = StaticText(font)

    val value get() = originalText

    init {
        background.color = Color(0xDD333333.toInt(), true)
        background.blendMode = BlendMode.ALPHA
        textDraw.isMultiLine = false
        textDraw.color = Color.WHITE
        border.color = Color.WHITE
        caretQuad.size = Vec2(2, 25)
        caretQuad.color = Color.WHITE
    }

    override fun onFrame(context: UiContext) {
        background.onFrame()

        context.clipStack.push(clipStart, clipEnd)
        textDraw.onFrame()
        context.clipStack.pop()

        checkCaretVisibility()
        if(hasFocus && caretVisible) {
            caretQuad.onFrame()
        }

        border.onFrame()
    }

    override fun handleMessage(message: WindowMessage) {
        super.handleMessage(message)
        val context = message.context

        if (message is MouseClickMessage) {
            val pos = message.position
            if (message.isHandled) {
                if (context.inputFocus == this) {
                    context.inputFocus = null
                }

                hasFocus = false
                return
            }

            if (message.button == MouseButton.LEFT) {
                val contained = pointInQuad(pos, calculatedPosition, bottomRight)
                if(contained) {
                    message.isHandled = true
                }
                if (message.pressed) {
                    if (contained) {
                        hasFocus = true
                        context.inputFocus = this
                    } else {
                        hasFocus = false
                        if (context.inputFocus == this) {
                            context.inputFocus = null
                        }
                    }
                }
            }
        } else if (message is KeyPressMessage) {
            if (hasFocus) {
                onCharacter(message.character, message.charCode.toInt())
            }
        }
    }

    private fun onCharacter(chr: Char, keyCode: Int) {
        when (keyCode) {
            GLFW_KEY_BACKSPACE -> onBackspace()
            GLFW_KEY_DELETE -> onDelete()
            GLFW_KEY_RIGHT -> moveCaretRight()
            GLFW_KEY_LEFT -> moveCaretLeft()
            in NON_PRINT_CHARACTER -> onNonPrintable()
            else -> onCharacter(chr)
        }

        updateCaretPosition()
    }

    private fun moveCaretLeft() {
        if(caretOffset > 0) {
            --caretOffset
            checkOnCaretMoveLeft()
        }

        caretVisible = true
        lastCaretUpdate = Date()
    }

    private fun moveCaretRight() {
        if(caretOffset < text.length) {
            ++caretOffset
            checkOnCaretMoveRight()
        }

        caretVisible = true
        lastCaretUpdate = Date()
    }

    private fun onBackspace() {
        if (caretOffset <= 0 || text.isEmpty()) {
            return
        }

        this.text = text.substring(0, caretOffset - 1) + text.substring(caretOffset)
        this.visibleText = visibleText.substring(0, (caretOffset - 1 - visibleTextStart)) + visibleText.substring(caretOffset - visibleTextStart)
        moveCaretLeft()
    }

    private fun onDelete() {
        if (caretOffset == text.length || text.isEmpty()) {
            return
        }
    }

    private fun onCharacter(chr: Char) {
        if (!font.canDisplay(chr)) {
            return
        }

        originalText = originalText.substring(0, caretOffset) + chr + originalText.substring(caretOffset)
        val realChar = convertCharacter(chr)
        val charLength = textMeasurer.getCharWidth(realChar)
        text = text.substring(0, caretOffset) + realChar + text.substring(caretOffset)
        ++caretOffset
        caretVisible = true
        lastCaretUpdate = Date()
        val newLength = textMeasurer.getTextLength(text)
        if (newLength < calculatedSize.x - 10) {
            visibleText = text
            visibleTextOffset = 5.0f
            visibleTextStart = 0
            return
        }

        if (caretOffset >= text.length) {
            var curLength = 0.0f
            var index = caretOffset - 1
            var newVisibleText = ""
            while (curLength < calculatedSize.x + 15 && index >= 0) {
                curLength += textMeasurer.getCharWidth(text[index])
                newVisibleText = text[index] + newVisibleText
                visibleTextStart = index
                --index
            }

            visibleTextOffset = calculatedSize.x - 5 - curLength
            visibleText = newVisibleText
            return
        }

        val oldOffset = caretOffset - 1
        val textBefore = text.substring(visibleTextStart, oldOffset)
        val oldLength = textMeasurer.getTextLength(textBefore)
        if (visibleTextOffset + oldLength + charLength < calculatedSize.x - 10) {
            var newText = textBefore + realChar
            var curLength = oldLength + charLength
            for (cur in visibleText.substring(oldOffset - visibleTextStart)) {
                newText += cur
                curLength += textMeasurer.getCharWidth(cur)
                if (visibleTextOffset + curLength > calculatedSize.x + 15) {
                    break
                }
            }
            visibleText = newText
        } else {
            centerCaret()
        }
    }

    private fun onNonPrintable() {

    }

    private fun visibleTextOffsetChanged() {
        textDraw.position = Vec2(calculatedPosition.x + visibleTextOffset, calculatedPosition.y + 5)
    }

    private fun visibleTextChanged() {
        textDraw.text = visibleText
    }

    override fun positionChanged() {
        super.positionChanged()
        visibleTextOffsetChanged()
        background.position = calculatedPosition
        background.size = calculatedSize
        bottomRight = calculatedPosition + calculatedSize
        clipStart = Vec2(calculatedPosition.x + 5, calculatedPosition.y + 6)
        clipEnd = Vec2(calculatedPosition.x + calculatedSize.x - 5, calculatedPosition.y + calculatedSize.y - 6)
        border.topLeft = calculatedPosition
        border.bottomRight = bottomRight
        updateCaretPosition()
    }

    override fun sizeChanged() {
        if(!sizeOverride) {
            sizeOverride = true
            size = Vec2(size.x, 35)
        } else {
            sizeOverride = false
        }

        super.sizeChanged()
        background.position = calculatedPosition
        background.size = calculatedSize
        bottomRight = calculatedPosition + calculatedSize
        clipStart = Vec2(calculatedPosition.x + 5, calculatedPosition.y + 6)
        clipEnd = Vec2(calculatedPosition.x + calculatedSize.x - 5, calculatedPosition.y + calculatedSize.y - 6)
        border.topLeft = calculatedPosition
        border.bottomRight = bottomRight
        updateCaretPosition()
    }

    private fun updateCaretPosition() {
        val textBeforeCaret = visibleText.substring(0, caretOffset - visibleTextStart)
        val caretOffset = visibleTextOffset + textMeasurer.getTextLength(textBeforeCaret)
        caretQuad.position = Vec2(calculatedPosition.x + caretOffset, calculatedPosition.y + 5)
    }

    private fun checkCaretVisibility() {
        val now = Date()
        val diff = now.time - lastCaretUpdate.time
        if(diff < 500) {
            return
        }

        caretVisible = !caretVisible
        lastCaretUpdate = now
    }

    private fun checkOnCaretMoveRight() {
        val textAfter = text.substring(visibleTextStart, caretOffset)
        val length = textMeasurer.getTextLength(textAfter)
        if((visibleTextOffset + length) < calculatedSize.x - 5) {
            return
        }

        centerCaret()
    }

    private fun checkOnCaretMoveLeft() {
        var caretPos = visibleTextOffset
        if(caretOffset < visibleTextStart) {
            val textBefore = text.substring(caretOffset, visibleTextStart)
            caretPos -= textMeasurer.getTextLength(textBefore)
        } else if(caretOffset > visibleTextStart) {
            val textAfter = text.substring(visibleTextStart, caretOffset)
            caretPos += textMeasurer.getTextLength(textAfter)
        }

        if(visibleTextStart <= 0) {
            return
        }

        if(caretPos <= 5) {
            centerCaret()
        }
    }

    private fun centerCaret() {
        var midPos = calculatedSize.x / 2.0f
        var length = 0.0f
        var index = caretOffset - 1
        var newText = ""
        while (length < (midPos + 15) && index < text.length) {
            length += textMeasurer.getCharWidth(text[index])
            newText += text[index]
            ++index
        }
        if(length < (calculatedSize.x - 10)) {
            midPos = calculatedSize.x - 5 - length
        }

        length = 0.0f
        index = caretOffset - 2
        while (length < (midPos + 15) && index >= 0) {
            length += textMeasurer.getCharWidth(text[index])
            newText = text[index] + newText
            visibleTextStart = index
            --index
        }

        visibleText = newText
        visibleTextOffset = midPos - length
        if(visibleTextOffset > 5) {
            visibleTextStart = 0
            length = 0.0f
            newText = ""
            index = 0
            while(length < (calculatedSize.x) && index < text.length) {
                length += textMeasurer.getCharWidth(text[index])
                newText += text[index]
                ++index
            }
            visibleText = newText
            visibleTextOffset = 5.0f
        }
    }

    internal open fun convertCharacter(chr: Char): Char {
        return chr
    }
}

class PasswordTextBox : TextBox() {
    val password get() = originalText

    override fun convertCharacter(chr: Char): Char {
        return '•'
    }
}