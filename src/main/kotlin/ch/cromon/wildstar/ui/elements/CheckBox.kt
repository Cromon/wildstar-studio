package ch.cromon.wildstar.ui.elements

import ch.cromon.wildstar.utils.pointInQuad
import ch.cromon.wildstar.ui.UiContext
import ch.cromon.wildstar.ui.messaging.MouseButton
import ch.cromon.wildstar.ui.messaging.MouseClickMessage
import ch.cromon.wildstar.ui.messaging.MouseMoveMessage
import ch.cromon.wildstar.ui.messaging.WindowMessage
import ch.cromon.wildstar.ui.text.StaticText
import glm.vec2.Vec2
import java.awt.Color
import java.awt.Font
import kotlin.properties.Delegates


class CheckBox : Element() {
    companion object {
        private val BOX_SIZE = Vec2(30, 30)
    }

    private val checkBorder = Border()
    private val checkText = StaticText(Font("Arial", Font.BOLD, 35))
    private val captionRender = StaticText(Font("Arial", Font.BOLD, 30))
    private var isHovered = false
    private var isClicked = false

    var caption by Delegates.observable("", { _, _, _ -> onTextChanged() })

    private var isChecked = false

    init {
        checkBorder.thickness = 3
        checkBorder.color = Color.WHITE
        checkText.text = "×"
    }

    override fun handleMessage(message: WindowMessage) {
        super.handleMessage(message)

        if(message is MouseMoveMessage) {
            handleMouseMove(message)
        } else if(message is MouseClickMessage) {
            handleMouseClick(message)
        }
    }

    private fun handleMouseClick(message: MouseClickMessage) {
        if(isHovered) {
            message.isHandled = true
        }

        if(message.button != MouseButton.LEFT) {
            return
        }

        if(!message.pressed) {
            if(isHovered && isClicked) {
                isChecked = !isChecked
            }

            isClicked = false
        } else {
            if(isHovered) {
                isClicked = true
            }
        }
    }

    private fun handleMouseMove(message: MouseMoveMessage) {
        if(!message.isHandled) {
            val isHovered = pointInQuad(message.position, calculatedPosition, calculatedPosition + BOX_SIZE)
            if(isHovered && !this.isHovered) {
                this.isHovered = true
                checkText.color = Color.ORANGE
                checkBorder.color = Color.ORANGE
            } else if(!isHovered && this.isHovered) {
                this.isHovered = false
                checkText.color = Color.WHITE
                checkBorder.color = Color.WHITE
            }

            if(isHovered) {
                message.isHandled = true
            }
        }
    }

    override fun onFrame(context: UiContext) {
        checkBorder.onFrame()
        if(isChecked) {
            checkText.onFrame()
        }
        captionRender.onFrame()
    }

    override fun positionChanged() {
        super.positionChanged()
        
        checkBorder.topLeft = calculatedPosition
        checkBorder.bottomRight = calculatedPosition + BOX_SIZE
        checkText.position = calculatedPosition + Vec2(5, -5)
        val textHeight = captionRender.textHeight
        captionRender.position = calculatedPosition + Vec2(BOX_SIZE.x + 5, (BOX_SIZE.y - textHeight) / 2)
    }

    private fun onTextChanged() {
        captionRender.text = caption
        val textHeight = captionRender.textHeight
        captionRender.position = calculatedPosition + Vec2(BOX_SIZE.x + 5, (BOX_SIZE.y - textHeight) / 2)
    }

}