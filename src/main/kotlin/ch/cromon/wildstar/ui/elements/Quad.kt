package ch.cromon.wildstar.ui.elements

import ch.cromon.wildstar.gx.*
import ch.cromon.wildstar.utils.toVec4
import glm.mat4x4.Mat4
import glm.vec2.Vec2
import glm.vec4.Vec4
import org.lwjgl.BufferUtils
import java.awt.Color
import kotlin.properties.Delegates

class Quad {
    private var matTransform = Mat4()
    private var colorTransformed = Vec4(1, 1, 1, 1)

    var position: Vec2 by Delegates.observable(Vec2(), { _, _, _ -> updateSizePosition() })
    var size: Vec2 by Delegates.observable(Vec2(), { _, _, _ -> updateSizePosition() })
    var color: Color by Delegates.observable(Color.WHITE, { _, _, _ -> colorTransformed = color.toVec4() })

    var blendMode = BlendMode.NONE

    fun onFrame() {
        mesh.program.setMatrix(uniformMatTransform, matTransform)
        mesh.program.setVec4(uniformColor, colorTransformed)
        mesh.blendMode = blendMode
        mesh.render()
    }

    private fun updateSizePosition() {
        matTransform = Mat4().translate(position.x, position.y, 0.0f).scale(size.x, size.y, 1.0f)
    }

    companion object {
        private lateinit var mesh: Mesh
        private var uniformMatTransform = -1
        private var uniformGlobalTransform = -1
        private var uniformMatProjection = -1
        private var uniformColor = -1

        fun updateProjection(matrix: Mat4) {
            mesh.program.setMatrix(uniformMatProjection, matrix)
        }

        fun updateGlobalTransform(matrix: Mat4) {
            mesh.program.setMatrix(uniformGlobalTransform, matrix)
        }

        fun initialize() {
            mesh = Mesh()
            val program = Program()
            program.compileVertexShaderFromResource(Shader.QUAD.vertexShader)
            program.compileFragmentShaderFromResource(Shader.QUAD.fragmentShader)
            program.link()

            uniformMatProjection = program.getUniform("matProjection")
            uniformMatTransform = program.getUniform("matTransform")
            uniformGlobalTransform = program.getUniform("globalTransform")
            uniformColor = program.getUniform("quadColor")

            mesh.program = program
            mesh.blendMode = BlendMode.NONE

            mesh.addElement(VertexElement("position", 0, 2))

            mesh.finalize()

            val vb = VertexBuffer()
            val vbData = BufferUtils.createFloatBuffer(4 * 2)
            vbData.put(0.0f).put(1.0f)
            vbData.put(1.0f).put(1.0f)
            vbData.put(1.0f).put(0.0f)
            vbData.put(0.0f).put(0.0f)
            vbData.flip()
            vb.data(vbData)

            val ib = IndexBuffer(IndexType.UINT8)
            val ibData = BufferUtils.createByteBuffer(6)
            ibData.put(0).put(1).put(2)
            ibData.put(0).put(2).put(3)
            ibData.flip()
            ib.data(ibData)

            mesh.vertexBuffer = vb
            mesh.indexBuffer = ib
            mesh.indexCount = 6
        }
    }
}