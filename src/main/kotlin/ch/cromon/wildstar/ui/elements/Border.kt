package ch.cromon.wildstar.ui.elements

import glm.vec2.Vec2
import java.awt.Color
import kotlin.properties.Delegates


class Border {
    private val left = Quad()
    private val right = Quad()
    private val top = Quad()
    private val bottom = Quad()

    var thickness: Int by Delegates.observable(1, { _, _, _ -> updateSizePosition() })
    var topLeft: Vec2 by Delegates.observable(Vec2(), { _, _, _ -> updateSizePosition() })
    var bottomRight: Vec2 by Delegates.observable(Vec2(), { _, _, _ -> updateSizePosition() })
    var color: Color by Delegates.observable(Color.WHITE, { _, _, _ -> updateColor() })

    var drawLeft = true
    var drawRight = true
    var drawTop = true
    var drawBottom = true

    fun onFrame() {
        if(drawLeft) left.onFrame()
        if(drawRight) right.onFrame()
        if(drawTop) top.onFrame()
        if(drawBottom) bottom.onFrame()
    }

    private fun updateColor() {
        left.color = color
        right.color = color
        top.color = color
        bottom.color = color
    }

    private fun updateSizePosition() {
        val w = bottomRight.x - topLeft.x
        val h = bottomRight.y - topLeft.y

        left.position = topLeft
        left.size = Vec2(thickness, h)

        right.position = Vec2(bottomRight.x - thickness, topLeft.y)
        right.size = Vec2(thickness, h)

        top.position = topLeft
        top.size = Vec2(w, thickness)

        bottom.position = Vec2(topLeft.x, bottomRight.y - thickness)
        bottom.size = Vec2(w, thickness)
    }
}