package ch.cromon.wildstar.ui.utils

import glm.mat4x4.Mat4
import org.slf4j.LoggerFactory
import java.util.*


class TransformStack(private val callback: (Mat4) -> Unit) {
    companion object {
        private val LOG = LoggerFactory.getLogger(TransformStack::class.java)
    }

    private val matrixStack = Stack<Mat4>()

    val currentMatrix: Mat4
        get() = matrixStack.peek()

    init {
        matrixStack.push(Mat4())
    }

    fun popMatrix() {
        if(matrixStack.size <= 1) {
            LOG.error("Attempted to pop from TransformStack with only the identity matrix left")
            throw IllegalStateException("Attempted to pop from empty matrix stack")
        }

        matrixStack.pop()
        callback(matrixStack.peek())
    }

    fun pushMatrix(matrix: Mat4) {
        val top = matrixStack.peek()
        val newTop = top * matrix
        matrixStack.push(newTop)
        callback(newTop)
    }
}