package ch.cromon.wildstar.ui.utils

import ch.cromon.wildstar.utils.mulVec2
import glm.mat4x4.Mat4
import glm.vec2.Vec2
import org.lwjgl.opengl.GL11.*
import org.slf4j.LoggerFactory
import java.util.*

private class StackEntry(val topLeft: Vec2, val bottomRight: Vec2)

class ClipStack {
    companion object {
        private val LOG = LoggerFactory.getLogger(ClipStack::class.java)
    }

    var windowHeight = 0

    private val clipStack = Stack<StackEntry>()

    var transformStack : TransformStack? = null

    fun pop() {
        if(clipStack.empty()) {
            LOG.error("Attempted to pop an entry from an empty ClipStack")
            throw IllegalStateException("Attempted to pop from an empty stack")
        }

        clipStack.pop()
        if(clipStack.empty()) {
            glDisable(GL_SCISSOR_TEST)
        } else {
            val top = clipStack.peek()
            updateScissorRect(top.topLeft, top.bottomRight)
        }
    }

    fun push(topLeft: Vec2, bottomRight: Vec2, ignoreTransform: Boolean = false) {
        val transform = transformStack?.currentMatrix ?: Mat4()
        val ttl = if(ignoreTransform) topLeft else transform.mulVec2(topLeft)
        val tbr = if(ignoreTransform) bottomRight else transform.mulVec2(bottomRight)

        if(clipStack.empty()) {
            pushEntry(ttl, tbr)
        } else {
            val curTop = clipStack.peek()
            val tl = curTop.topLeft
            val br = curTop.bottomRight
            val minX = minOf(tl.x, ttl.x)
            val maxX = maxOf(br.x, tbr.x)
            val minY = minOf(tl.y, ttl.y)
            val maxY = maxOf(br.y, tbr.y)
            pushEntry(Vec2(minX, minY), Vec2(maxX, maxY))
        }
    }

    private fun pushEntry(topLeft: Vec2, bottomRight: Vec2) {
        val entry = StackEntry(topLeft, bottomRight)
        if(clipStack.empty()) {
            glEnable(GL_SCISSOR_TEST)
        }

        clipStack.push(entry)
        updateScissorRect(topLeft, bottomRight)
    }

    private fun updateScissorRect(topLeft: Vec2, bottomRight: Vec2) {
        val w = (bottomRight.x - topLeft.x).toInt()
        val h = (bottomRight.y - topLeft.y).toInt()

        glScissor(topLeft.x.toInt(), (windowHeight - topLeft.y - h).toInt(), w, h)
    }
}