package ch.cromon.wildstar.ui.utils


object ColorUtils {
    fun hsvToRgb(h: Float, s: Float, v: Float): Int {
        val bigH = h * 360
        var r = 0
        var g = 0
        var b = 0
        val a = 0xFF

        val c = v * s
        val x = c * (1 - Math.abs((bigH / 60.0) % 2.0 - 1))
        val m = v - c
        when ((bigH / 60.0).toInt()) {
            0 -> {
                r = Math.floor((c + m) * 255.0).toByte().toInt()
                g = Math.floor((x + m) * 255.0).toByte().toInt()
                b = Math.floor(m * 255.0).toByte().toInt()
            }
            1 -> {
                r = Math.floor((x + m) * 255.0).toByte().toInt()
                g = Math.floor((c + m) * 255.0).toByte().toInt()
                b = Math.floor(m * 255.0).toByte().toInt()
            }
            2 -> {
                r = Math.floor(m * 255.0).toByte().toInt()
                g = Math.floor((c + m) * 255.0).toByte().toInt()
                b = Math.floor((x + m) * 255.0).toByte().toInt()
            }
            3 -> {
                r = Math.floor(m * 255.0).toByte().toInt()
                g = Math.floor((x + m) * 255.0).toByte().toInt()
                b = Math.floor((c + m) * 255.0).toByte().toInt()
            }
            4 -> {
                r = Math.floor((x + m) * 255.0).toByte().toInt()
                g = Math.floor(m * 255.0).toByte().toInt()
                b = Math.floor((c + m) * 255.0).toByte().toInt()
            }
            5 -> {
                r = Math.floor((c + m) * 255.0).toByte().toInt()
                g = Math.floor(m * 255.0).toByte().toInt()
                b = Math.floor((x + m) * 255.0).toByte().toInt()
            }
        }

        return ((a and 0xFF) shl 24) or ((r and 0xFF) shl 16) or ((g and 0xFF) shl 8) or ((b and 0xFF))
    }
}