package ch.cromon.wildstar.ui.utils

import java.util.*


object FpsCounter {
    private var lastUpdate = Date()
    private var numFrames = 0
    private var lastFps = 0.0f

    val fps get() = lastFps

    fun onFrame() {
        ++numFrames
        val now = Date()
        val diff = now.time - lastUpdate.time
        if(diff >= 1000) {
            lastFps = (numFrames / diff.toFloat()) * 1000.0f
            numFrames = 0
            lastUpdate = now
        }
    }
}