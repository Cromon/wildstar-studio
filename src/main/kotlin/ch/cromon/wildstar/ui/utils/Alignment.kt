package ch.cromon.wildstar.ui.utils

import ch.cromon.wildstar.ui.elements.Element
import ch.cromon.wildstar.ui.elements.ItemsElement
import glm.vec2.Vec2


enum class Alignment {
    START,
    END,
    CENTER,
    STRETCH
}

class AlignmentResult(var position: Vec2 = Vec2(), var size: Vec2 = Vec2())

object AlignmentManager {
    fun calculate(parent: ItemsElement?, element: Element, horizontalAlignment: Alignment, verticalAlignment: Alignment): AlignmentResult {
        if(parent == null) {
            return AlignmentResult(element.position, element.size)
        }

        val offset = parent.getChildOffset()
        val px = 0
        val py = 0
        val ex = element.position.x
        val ey = element.position.y
        val psx = parent.calculatedSize.x - 2 * offset.x
        val psy = parent.calculatedSize.y - offset.y
        val esx = element.size.x
        val esy = element.size.y

        return when(horizontalAlignment) {
            Alignment.START -> {
                when(verticalAlignment) {
                    Alignment.START -> AlignmentResult(element.position, element.size)
                    Alignment.CENTER -> {
                        val posY = py + (psy - esy) / 2.0f + ey
                        return AlignmentResult(Vec2(ex, posY), element.size)
                    }
                    Alignment.STRETCH -> {
                        return AlignmentResult(Vec2(ex, py), Vec2(esx, esy))
                    }
                    Alignment.END -> {
                        val posY = psy - ey
                        return AlignmentResult(Vec2(ex, posY), element.size)
                    }
                }
            }
            Alignment.STRETCH -> {
                when(verticalAlignment) {
                    Alignment.START -> {
                        return AlignmentResult(Vec2(px, ey), Vec2(psx, esy))
                    }
                    Alignment.CENTER -> {
                        val posY = py + (psy - esy) / 2.0f
                        return AlignmentResult(Vec2(px, posY), Vec2(psx, esy))
                    }
                    Alignment.STRETCH -> {
                        return AlignmentResult(Vec2(px, py), Vec2(psx, psy))
                    }
                    Alignment.END -> {
                        val posY = psy - esy
                        return AlignmentResult(Vec2(px, posY), Vec2(psx, esy))
                    }
                }
            }
            Alignment.END -> {
                when(verticalAlignment) {
                    Alignment.START -> {
                        val posX = parent.size.x - element.size.x
                        return AlignmentResult(Vec2(posX, element.position.y), element.size)
                    }
                    Alignment.CENTER -> {
                        val posX = parent.size.x - element.size.x
                        val posY = parent.position.y + (parent.size.y - element.size.y) / 2.0f
                        return AlignmentResult(Vec2(posX, posY), element.size)
                    }
                    Alignment.STRETCH -> {
                        val posX = parent.size.x - element.size.x
                        return AlignmentResult(Vec2(posX, 0), Vec2(element.size.x, parent.size.y))
                    }
                    Alignment.END -> {
                        val posX = parent.size.x - element.size.x
                        val posY = parent.size.y - element.size.y
                        return AlignmentResult(Vec2(posX, posY), element.size)
                    }
                }
            }
            Alignment.CENTER -> {
                when(verticalAlignment) {
                    Alignment.START -> {
                        val posX = (parent.size.x - element.size.x) / 2.0f + element.position.x
                        val posY = element.position.y
                        return AlignmentResult(Vec2(posX, posY), element.size)
                    }
                    Alignment.CENTER -> {
                        val posX = (parent.size.x - element.size.x) / 2.0f + element.position.x
                        val posY = (parent.size.y - element.size.y) / 2.0f + element.position.y
                        return AlignmentResult(Vec2(posX, posY), element.size)
                    }
                    Alignment.STRETCH -> {
                        val posX = (parent.size.x - element.size.x) / 2.0f + element.position.x
                        return AlignmentResult(Vec2(posX, 0), Vec2(element.size.x, parent.size.y))
                    }
                    Alignment.END -> {
                        val posX = (parent.size.x - element.size.x) / 2.0f + element.position.x
                        val posY = parent.size.y - element.size.y
                        return AlignmentResult(Vec2(posX, posY), element.size)
                    }
                }
            }
        }
    }
}