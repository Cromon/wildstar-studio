package ch.cromon.wildstar.ui

import ch.cromon.wildstar.ui.elements.Element
import ch.cromon.wildstar.ui.utils.ClipStack
import ch.cromon.wildstar.ui.utils.TransformStack


class UiContext(val clipStack: ClipStack, val transformStack: TransformStack) {
    init {
        clipStack.transformStack = transformStack
    }

    var inputFocus: Element? = null
}