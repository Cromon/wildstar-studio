package ch.cromon.wildstar.ui.views.files

import ch.cromon.wildstar.gx.*
import ch.cromon.wildstar.io.ImageSource
import ch.cromon.wildstar.io.LittleEndianStream
import ch.cromon.wildstar.lexer.BaseLexer
import ch.cromon.wildstar.lexer.RenderedLine
import ch.cromon.wildstar.lexer.TextLexer
import ch.cromon.wildstar.ui.UiContext
import ch.cromon.wildstar.ui.elements.Quad
import glm.mat4x4.Mat4
import glm.vec2.Vec2
import org.apache.commons.lang3.StringUtils.isBlank
import org.lwjgl.BufferUtils
import org.slf4j.Logger
import org.springframework.beans.factory.NoSuchBeanDefinitionException
import org.springframework.beans.factory.NoUniqueBeanDefinitionException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.BeanFactoryAnnotationUtils
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component
import java.awt.Color
import java.io.File
import java.nio.charset.StandardCharsets

@Component
@Scope("prototype")
class TextFileView : FileView() {
    companion object {
        private val LEXER_MAP = mapOf(
                "xml" to "xml",
                "form" to "xml",
                "lua" to "lua"
        )

        private val DEFAULT_LEXER = "text"
    }

    @Autowired
    private lateinit var logger: Logger

    @Autowired
    private lateinit var context: ApplicationContext

    private lateinit var lexer: BaseLexer

    private lateinit var background: Quad

    private val parsedLines = ArrayList<RenderedLine>()
    private val renderLines = ArrayList<LineDrawer>()

    private var scrollOffset = 0.0f
    private var numLines = 0

    private var clipTopLeft = Vec2()
    private var clipBottomRight = Vec2()

    override fun onInitialize() {
        super.onInitialize()

        background = Quad()
        background.blendMode = BlendMode.NONE
        background.color = Color(0x2222)
    }

    override fun onFrame(context: UiContext) {
        if(numLines == 0) {
            return
        }

        background.onFrame()

        context.clipStack.push(clipTopLeft, clipBottomRight)

        var currentPos = -scrollOffset
        for(i in 0 until numLines) {
            if(i >= parsedLines.size) {
                parsedLines.add(lexer.drawLine(i, calculatedSize.x.toInt() - 20))
                renderLines.add(LineDrawer(parsedLines[i]))
            }

            val line = parsedLines[i]
            val drawer = renderLines[i]
            if(currentPos + line.bounds.y < 0) {
                currentPos += line.bounds.y
                continue
            }

            if(currentPos > calculatedSize.y + 50) {
                break
            }

            if(drawer.isEmpty) {
                currentPos += line.bounds.y
                continue
            }

            drawer.onFrame(Vec2(calculatedPosition.x + 10, calculatedPosition.y + currentPos))
            currentPos += line.bounds.y
        }

        context.clipStack.pop()
    }

    override fun initialize(content: LittleEndianStream, fileName: String) {
        val textContent = String(content.data, StandardCharsets.UTF_8)
        val file = File(fileName)
        val extension = file.extension
        val lexerQualifier = if (isBlank(extension)) {
            DEFAULT_LEXER
        } else {
            LEXER_MAP.getOrDefault(extension.toLowerCase(), DEFAULT_LEXER)
        }

        lexer = getLexer(lexerQualifier)
        lexer.lex(textContent)
        numLines = lexer.getLineCount()
    }

    private fun getLexer(qualifier: String): BaseLexer {
        val factory = context.autowireCapableBeanFactory
        try {
            return BeanFactoryAnnotationUtils.qualifiedBeanOfType(factory, BaseLexer::class.java, "lexer.$qualifier")
        } catch (e: NoUniqueBeanDefinitionException) {
            logger.warn("Multiple lexers found for qualifier {}. Beans: {}", qualifier, e.beanNamesFound?.joinToString())
        } catch (e: NoSuchBeanDefinitionException) {
            logger.warn("No lexer found for qualifier {}.", qualifier)
        }

        return getDefaultLexer()
    }

    private fun getDefaultLexer(): TextLexer {
        return context.getBean(TextLexer::class.java)
    }

    override fun sizeChanged() {
        super.sizeChanged()

        parsedLines.clear()
        sizePositionChanged()
    }

    override fun positionChanged() {
        super.positionChanged()
        sizePositionChanged()
    }

    private fun sizePositionChanged() {
        clipTopLeft = calculatedPosition
        clipBottomRight = calculatedPosition + calculatedSize
        background.position = calculatedPosition
        background.size = calculatedSize
    }
}

class LineDrawer(private val line: RenderedLine) {
    private val texture = Texture()

    val isEmpty: Boolean = line.content.isBlank()

    private val matScale = Mat4().scale(line.bounds.x, line.bounds.y, 1.0f)

    init {
        texture.loadArgb(ImageSource.fromBufferedImage(line.image))
    }

    fun onFrame(position: Vec2) {
        mesh.textureInput.add(uniformTextTexture, texture)
        mesh.program.setMatrix(uniformMatTranslate, Mat4().translate(position.x, position.y, 0.0f))
        mesh.program.setMatrix(uniformMatTransform, matScale)
        mesh.program.setVec2(uniformDimension, line.bounds)

        mesh.render()
    }

    companion object {
        private var uniformMatTranslate = -1
        private var uniformMatTransform = -1
        private var uniformProjection = -1
        private var uniformTextTexture = -1
        private var uniformGlobalTransform = -1
        private var uniformDimension = -1

        private lateinit var mesh: Mesh

        fun updateProjection(matrix: Mat4) {
            mesh.program.setMatrix(uniformProjection, matrix)
        }

        fun updateGlobalTransform(matrix: Mat4) {
            mesh.program.setMatrix(uniformGlobalTransform, matrix)
        }

        fun initialize() {
            mesh = Mesh()

            val program = Program()
            program.compileFromResource(Shader.SYNTAX_TEXT)
            program.link()

            uniformMatTranslate = program.getUniform("matTranslate")
            uniformMatTransform = program.getUniform("matTransform")
            uniformProjection = program.getUniform("matProjection")
            uniformTextTexture = program.getUniform("textTexture")
            uniformGlobalTransform = program.getUniform("globalTransform")
            uniformDimension = program.getUniform("dimension")

            mesh.blendMode = BlendMode.ALPHA
            mesh.program = program

            val vb = VertexBuffer()
            val vbData = BufferUtils.createFloatBuffer(4 * 4)
            vbData.put(0.0f).put(1.0f).put(0.0f).put(1.0f)
            vbData.put(1.0f).put(1.0f).put(1.0f).put(1.0f)
            vbData.put(1.0f).put(0.0f).put(1.0f).put(0.0f)
            vbData.put(0.0f).put(0.0f).put(0.0f).put(0.0f)
            vbData.flip()
            vb.data(vbData)

            val ib = IndexBuffer(IndexType.UINT8)
            val ibData = BufferUtils.createByteBuffer(6)
            ibData.put(0).put(1).put(2)
            ibData.put(0).put(2).put(3)
            ibData.flip()
            ib.data(ibData)

            mesh.vertexBuffer = vb
            mesh.indexBuffer = ib
            mesh.indexCount = 6

            mesh.finalize()
        }
    }
}