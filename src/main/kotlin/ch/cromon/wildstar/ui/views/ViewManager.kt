package ch.cromon.wildstar.ui.views

import ch.cromon.wildstar.ApplicationState
import ch.cromon.wildstar.StateManager
import ch.cromon.wildstar.ui.elements.Element
import ch.cromon.wildstar.ui.elements.ItemsElement
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct

@Component
class ViewManager {
    private val viewMap = HashMap<ApplicationState, Collection<Element>>()

    private lateinit var uiRoot: ItemsElement
    private var activeElements: Collection<Element>? = null

    @Autowired
    private lateinit var fileSelect: FileSelectView

    @Autowired
    private lateinit var mainView: MainView

    @PostConstruct
    fun initialize() {
        viewMap.put(ApplicationState.FILE_SELECT, arrayListOf(fileSelect))
        viewMap.put(ApplicationState.LOAD_DONE, arrayListOf(mainView))
    }

    fun attach(root: ItemsElement) {
        uiRoot = root
        switchToState(StateManager.currentState)

        StateManager.stateChanged += { switchToState(it) }

        fileSelect.onInitialize()
        mainView.onInitialize()
    }

    private fun switchToState(state: ApplicationState) {
        activeElements?.forEach { uiRoot.removeChild(it) }
        activeElements = viewMap[state]
        activeElements?.forEach { uiRoot.appendChild(it) }
    }
}