package ch.cromon.wildstar.ui.views

import ch.cromon.wildstar.io.FileLoadCompleteEvent
import ch.cromon.wildstar.io.FileManager
import ch.cromon.wildstar.io.archive.FileEntry
import ch.cromon.wildstar.ui.UiContext
import ch.cromon.wildstar.ui.elements.Border
import ch.cromon.wildstar.ui.elements.ItemsElement
import ch.cromon.wildstar.ui.elements.LazyTreeView
import ch.cromon.wildstar.ui.elements.Quad
import ch.cromon.wildstar.ui.utils.Alignment
import ch.cromon.wildstar.ui.views.files.FileView
import ch.cromon.wildstar.ui.views.files.TextFileView
import glm.vec2.Vec2
import org.apache.commons.lang3.StringUtils
import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import java.awt.Color
import java.io.File


@Component
class MainView : ItemsElement() {
    private lateinit var topLeftBackground: Quad
    private lateinit var bottomBackground: Quad
    private lateinit var topBackground: Quad

    private lateinit var topBorder: Border
    private lateinit var bottomBorder: Border

    private lateinit var fileTreeView: LazyTreeView

    @Autowired
    private lateinit var logger: Logger

    @Autowired
    private lateinit var fileManager: FileManager

    @Autowired
    private lateinit var context: ApplicationContext

    private var activeFileView: FileView? = null
    private var newFileView: FileView? = null
    private var hasViewChanged = false

    private var textChanged = false

    override fun onFrame(context: UiContext) {
        if(textChanged) {
            sizePositionChanged()
            textChanged = false
        }

        if(hasViewChanged) {
            activeFileView?.let { removeChild(it) }
            activeFileView = newFileView
            newFileView = null
            hasViewChanged = false
            activeFileView?.let {
                appendChild(it)
                it.position = Vec2(calculatedPosition.x + 250, calculatedPosition.y + 250)
                it.size = Vec2(calculatedSize.x - 250, calculatedSize.y - 250)
            }
        }

        topLeftBackground.onFrame()
        bottomBackground.onFrame()
        topBackground.onFrame()

        super.onFrame(context)
        topBorder.onFrame()
        bottomBorder.onFrame()
    }

    override fun onInitialize() {
        super.onInitialize()

        topLeftBackground = Quad()
        bottomBackground = Quad()
        topBackground = Quad()

        topBorder = Border()
        bottomBorder = Border()

        initBackgrounds()

        horizontalAlignment = Alignment.STRETCH
        verticalAlignment = Alignment.STRETCH

        fileTreeView = LazyTreeView()
        appendChild(fileTreeView)

        fileTreeView.position = Vec2(10, 260)
        fileTreeView.size = Vec2(230, 100)
        fileTreeView.fileClicked += { onFileSelected(it) }
    }

    override fun positionChanged() {
        super.positionChanged()

        sizePositionChanged()
    }

    override fun sizeChanged() {
        super.sizeChanged()

        sizePositionChanged()
    }

    private fun sizePositionChanged() {
        bottomBackground.position = Vec2(0, 250)
        bottomBackground.size = Vec2(250, calculatedSize.y - 250)
        bottomBorder.topLeft = calculatedPosition + Vec2(0, 250)
        bottomBorder.bottomRight = calculatedPosition + Vec2(250, calculatedSize.y)

        topLeftBackground.position = Vec2(0, 0)
        topLeftBackground.size = Vec2(250, 250)

        topBackground.position = Vec2(250, 0)
        topBackground.size = Vec2(calculatedSize.x - 250, 250)

        topBorder.topLeft = Vec2(250, 0)
        topBorder.bottomRight = Vec2(calculatedSize.x, 250)

        fileTreeView.size = Vec2(230, calculatedSize.y - 260)

        activeFileView?.let {
            it.position = Vec2(calculatedPosition.x + 250, calculatedPosition.y + 250)
            it.size = Vec2(calculatedSize.x - 250, calculatedSize.y - 250)
        }
    }

    private fun initBackgrounds() {
        topBorder.drawBottom = true
        topBorder.drawLeft = false
        topBorder.drawRight = false
        topBorder.drawTop = false

        bottomBorder.drawRight = true
        bottomBorder.drawBottom = false
        bottomBorder.drawLeft = false
        bottomBorder.drawTop = false

        bottomBackground.color = Color(0x666666)
        topLeftBackground.color = Color(0x666666)
        topBackground.color = Color(0x666666)
    }

    @EventListener
    private fun onFileLoadComplete(event: FileLoadCompleteEvent) {
        fileTreeView.addAll(fileManager.getRootChildren())
    }

    private fun onFileSelected(entry: FileEntry) {
        fileManager.openFile(entry).handle({
            content, err ->
                val extension = File(entry.name).extension
            if(StringUtils.isBlank(extension)) {
                logger.warn("Unsupported file without extension: {}. Display not possible", entry.getFullPath())
                return@handle
            }

            val viewClass = FILE_VIEW_MAP[extension.toLowerCase()]
            if(viewClass == null) {
                logger.warn("File extension {} not supported/implemented yet. File: {}", extension, entry.getFullPath())
                return@handle
            }

            val view = context.getBean(viewClass)
            view.initialize(content, entry.getFullPath())
            newFileView = view
            hasViewChanged = true
        })
    }

    companion object {
        private val FILE_VIEW_MAP = mapOf(
                "lua" to TextFileView::class.java,
                "xml" to TextFileView::class.java,
                "form" to TextFileView::class.java,
                "txt" to TextFileView::class.java
        )
    }
}