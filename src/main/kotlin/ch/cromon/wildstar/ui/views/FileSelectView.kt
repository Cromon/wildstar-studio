package ch.cromon.wildstar.ui.views

import ch.cromon.wildstar.ApplicationState
import ch.cromon.wildstar.StateManager
import ch.cromon.wildstar.io.FileManager
import ch.cromon.wildstar.io.ImageSource
import ch.cromon.wildstar.io.archive.cdn.ArchiveLoadEvent
import ch.cromon.wildstar.ui.UiContext
import ch.cromon.wildstar.ui.elements.*
import ch.cromon.wildstar.ui.utils.Alignment
import glm.vec2.Vec2
import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import java.awt.Color
import java.io.File
import javax.swing.JFileChooser
import javax.swing.filechooser.FileFilter


@Component
class FileSelectView : ItemsElement() {
    private lateinit var topLeftBackground: Quad
    private lateinit var bottomBackground: Quad
    private lateinit var topBackground: Quad

    private lateinit var topBorder: Border
    private lateinit var bottomBorder: Border

    private lateinit var selectCdnButton: MainMenuButton
    private lateinit var selectLocalButton: MainMenuButton

    private lateinit var initProgressLabel: Label

    @Autowired
    private lateinit var logger: Logger

    @Autowired
    private lateinit var fileManager: FileManager

    private var textChanged = false

    override fun onFrame(context: UiContext) {
        if(textChanged) {
            sizePositionChanged()
            textChanged = false
        }

        topLeftBackground.onFrame()
        bottomBackground.onFrame()
        topBackground.onFrame()

        super.onFrame(context)
        topBorder.onFrame()
        bottomBorder.onFrame()
    }

    override fun onInitialize() {
        super.onInitialize()

        topLeftBackground = Quad()
        bottomBackground = Quad()
        topBackground = Quad()

        topBorder = Border()
        bottomBorder = Border()

        initBackgrounds()

        horizontalAlignment = Alignment.STRETCH
        verticalAlignment = Alignment.STRETCH

        selectCdnButton = MainMenuButton()
        selectLocalButton = MainMenuButton()
        initProgressLabel = Label()

        appendChild(selectCdnButton)
        appendChild(selectLocalButton)
        appendChild(initProgressLabel)

        initProgressLabel.fontSize = 35

        initButtons()
    }

    override fun positionChanged() {
        super.positionChanged()

        sizePositionChanged()
    }

    override fun sizeChanged() {
        super.sizeChanged()

        sizePositionChanged()
    }

    private fun sizePositionChanged() {
        bottomBackground.position = Vec2(0, 250)
        bottomBackground.size = Vec2(250, calculatedSize.y - 250)
        bottomBorder.topLeft = calculatedPosition + Vec2(0, 250)
        bottomBorder.bottomRight = calculatedPosition + Vec2(250, calculatedSize.y)

        topLeftBackground.position = Vec2(0, 0)
        topLeftBackground.size = Vec2(250, 250)

        topBackground.position = Vec2(250, 0)
        topBackground.size = Vec2(calculatedSize.x - 250, 250)

        topBorder.topLeft = Vec2(250, 0)
        topBorder.bottomRight = Vec2(calculatedSize.x, 250)

        val regularSize = calculatedSize.x - 250
        val regularHeight = calculatedSize.y - 250

        val textWidth = initProgressLabel.textWidth
        val textHeight = initProgressLabel.textHeight

        initProgressLabel.position = Vec2(250 + (regularSize - textWidth) / 2.0f, 250 + (regularHeight - textHeight) / 2.0f)
    }

    private fun initButtons() {
        selectCdnButton.position = Vec2(25, 30)
        selectCdnButton.size = Vec2(170, 190)
        selectCdnButton.imageSource = ImageSource.fromImageResource("icon_cloud_white.png")
        selectCdnButton.text = "Load CDN"

        selectLocalButton.position = Vec2(225, 30)
        selectLocalButton.size = Vec2(170, 190)
        selectLocalButton.imageSource = ImageSource.fromImageResource("icon_computer_white.png")
        selectLocalButton.text = "Load Local"

        selectCdnButton.onClick += { onSelectFromCdn() }
        selectLocalButton.onClick += { onSelectFromLocal() }
    }

    private fun initBackgrounds() {
        topBorder.drawBottom = true
        topBorder.drawLeft = false
        topBorder.drawRight = false
        topBorder.drawTop = false

        bottomBorder.drawRight = true
        bottomBorder.drawBottom = false
        bottomBorder.drawLeft = false
        bottomBorder.drawTop = false

        bottomBackground.color = Color(0x666666)
        topLeftBackground.color = Color(0x666666)
        topBackground.color = Color(0x666666)
    }

    private fun onSelectFromCdn() {
        selectCdnButton.disabled = true
        selectLocalButton.disabled = true
        fileManager.initFromCdn().handle({
            _, error ->
            if(error != null) {
                initProgressLabel.text = error.localizedMessage
                selectCdnButton.disabled = false
                selectLocalButton.disabled = false
            } else {
                StateManager.currentState = ApplicationState.LOAD_DONE
            }
        })
    }

    private fun onSelectFromLocal() {
        selectCdnButton.disabled = true
        selectLocalButton.disabled = true
        val fileChooser = JFileChooser()
        fileChooser.addChoosableFileFilter(object : FileFilter() {
            override fun accept(f: File?): Boolean {
                return f?.name == "ClientData.index" || f?.isDirectory == true
            }

            override fun getDescription(): String {
                return "ClientData.index"
            }

        })

        fileChooser.isAcceptAllFileFilterUsed = false

        if(fileChooser.showOpenDialog(null) != JFileChooser.APPROVE_OPTION) {
            selectCdnButton.disabled = false
            selectLocalButton.disabled = false
            return
        }

        val selectedFile = fileChooser.selectedFile
        logger.info("Selected file: {}", selectedFile.absolutePath)
        fileManager.initFromLocal(selectedFile).handle({
            _, error ->
            if(error != null) {
                initProgressLabel.text = error.localizedMessage
                selectCdnButton.disabled = false
                selectLocalButton.disabled = false
            } else {
                StateManager.currentState = ApplicationState.LOAD_DONE
            }
        })
    }

    @EventListener
    private fun onInitProgressChanged(event: ArchiveLoadEvent) {
        initProgressLabel.text = event.message
        textChanged = true
    }
}