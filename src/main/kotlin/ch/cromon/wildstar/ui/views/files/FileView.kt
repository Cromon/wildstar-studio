package ch.cromon.wildstar.ui.views.files

import ch.cromon.wildstar.io.LittleEndianStream
import ch.cromon.wildstar.ui.elements.Element


abstract class FileView : Element() {
    abstract fun initialize(content: LittleEndianStream, fileName: String)
}