package ch.cromon.wildstar.ui.messaging

import ch.cromon.wildstar.ui.UiContext
import glm.vec2.Vec2


abstract class WindowMessage(val context: UiContext) {
    open fun forwardMessage(position: Vec2): WindowMessage = this

    var isHandled = false
}

enum class MouseButton {
    LEFT,
    RIGHT,
    MIDDLE,
    UNKNOWN
}

open class MouseMessage(val position: Vec2, context: UiContext) : WindowMessage(context)

class MouseClickMessage(position: Vec2, val button: MouseButton, val pressed: Boolean, context: UiContext) : MouseMessage(position, context) {
    override fun forwardMessage(position: Vec2): WindowMessage {
        val ret = MouseClickMessage(this.position - position, button, pressed, context)
        ret.isHandled = this.isHandled
        return ret
    }
}

class MouseMoveMessage(position: Vec2, context: UiContext) : MouseMessage(position, context) {
    override fun forwardMessage(position: Vec2): WindowMessage {
        val ret = MouseMoveMessage(this.position - position, context)
        ret.isHandled = this.isHandled
        return ret
    }
}

open class KeyboardMessage(val character: Char, val charCode: Short, context: UiContext) : WindowMessage(context)

class KeyPressMessage(chr: Char, chrCode: Short, context: UiContext): KeyboardMessage(chr, chrCode, context)

class KeyReleaseMessage(chr: Char, chrCode: Short, context: UiContext): KeyboardMessage(chr, chrCode, context)

class MouseWheelMessage(position: Vec2, val dx: Float, val dy: Float, context: UiContext) : MouseMessage(position, context)