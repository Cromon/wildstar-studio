package ch.cromon.wildstar.ui.text

import glm.vec2.Vec2
import java.awt.Color
import java.awt.Font
import java.awt.RenderingHints
import java.awt.font.LineBreakMeasurer
import java.awt.font.TextAttribute
import java.awt.image.BufferedImage
import java.text.AttributedString
import javax.swing.JLabel

class LoadResult(val bufferedImage: BufferedImage, val bounds: Vec2)

class TextLoader(private val font: Font) {
    private val label = JLabel()
    private val fontMetrics = label.getFontMetrics(font)
    private val graphics = BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB).createGraphics()

    fun drawText(text: String): LoadResult {
        val metrics = fontMetrics.getLineMetrics(text, graphics)
        val bounds = fontMetrics.getStringBounds(text, graphics)

        val bufferedImage = BufferedImage(bounds.width.toInt(), bounds.height.toInt(), BufferedImage.TYPE_INT_ARGB)
        val g = bufferedImage.createGraphics()
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON)
        g.font = font
        g.color = Color.WHITE
        g.drawString(text, 0, metrics.ascent.toInt())
        return LoadResult(bufferedImage, Vec2(bounds.width.toInt(), bounds.height.toInt()))
    }

    fun drawTextMultiline(text: String, width: Int): LoadResult {
        val attributedString = AttributedString(text)
        attributedString.addAttribute(TextAttribute.FONT, font)
        val iterator = attributedString.iterator
        val start = iterator.beginIndex
        val end = iterator.endIndex

        val measurer = LineBreakMeasurer(iterator, graphics.fontRenderContext)
        measurer.position = start
        var sizeY = 0.0f
        while(measurer.position < end) {
            val layout = measurer.nextLayout(width.toFloat())
            sizeY += layout.ascent + layout.descent + layout.leading
        }

        val img = BufferedImage(width, sizeY.toInt(), BufferedImage.TYPE_INT_ARGB)
        measurer.position = start

        val g = img.createGraphics()
        g.font = font
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON)
        val x = 0.0f
        var y = 0.0f

        while(measurer.position < end) {
            val layout = measurer.nextLayout(width.toFloat())
            y += layout.ascent
            val dx = if(layout.isLeftToRight) 0.0f else width - layout.advance
            layout.draw(g, x + dx, y)
            y += layout.descent + layout.leading
        }

        return LoadResult(img, Vec2(width, img.height))
    }
}