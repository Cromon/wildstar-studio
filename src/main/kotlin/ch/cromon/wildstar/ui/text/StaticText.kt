package ch.cromon.wildstar.ui.text

import ch.cromon.wildstar.gx.*
import ch.cromon.wildstar.io.ImageSource
import ch.cromon.wildstar.utils.toVec4
import glm.mat4x4.Mat4
import glm.vec2.Vec2
import org.lwjgl.BufferUtils
import java.awt.Color
import java.awt.Font
import kotlin.properties.Delegates


class StaticText(font: Font) {
    private val loader = TextLoader(font)
    var isMultiLine by Delegates.observable(false, { _, _, _ -> onPropertyUpdated() })
    var text by Delegates.observable(null as String?, { _, _, _ -> onPropertyUpdated() })
    private var isEmpty = true
    private var maxWidth by Delegates.observable(-1.0f, { _, _, _ -> onPropertyUpdated() })
    var textWidth = 0.0f
    var textHeight = 0.0f
    private val texture = Texture()
    private var matTransform = Mat4()
    private var matTranslate = Mat4()
    private var textColor = Color.WHITE.toVec4()
    var position: Vec2 by Delegates.observable(Vec2(), { _, _, _ -> matTranslate = Mat4().translate(position.x, position.y, 0.0f) })
    private var lastLoadResult: LoadResult? = null
    private var dimension = Vec2(1, 1)

    var color: Color = Color.WHITE
        set(value) { textColor = value.toVec4() }

    fun onFrame() {
        if (isEmpty) {
            return
        }

        lastLoadResult?.let {
            texture.loadArgb(ImageSource.fromBufferedImage(it.bufferedImage))
            lastLoadResult = null
        }

        mesh.program.setMatrix(uniformMatTranslate, matTranslate)
        mesh.program.setMatrix(uniformMatTransform, matTransform)
        mesh.textureInput.add(uniformTextTexture, texture)
        mesh.program.setVec4(uniformColor, textColor)
        mesh.program.setVec2(uniformDimension, dimension)
        mesh.render()
    }

    private fun onPropertyUpdated() {
        val actualText = text ?: ""
        if (actualText.isBlank()) {
            textWidth = 0.0f
            isEmpty = true
            return
        }

        isEmpty = false

        val result =
                if (!isMultiLine || maxWidth < 0)
                    loader.drawText(actualText)
                else
                    loader.drawTextMultiline(actualText, maxWidth.toInt())

        lastLoadResult = result
        matTransform = Mat4().scale(result.bounds.x, result.bounds.y, 1.0f)
        textWidth = result.bounds.x
        textHeight = result.bounds.y
        dimension = Vec2(textWidth, textHeight)
    }

    companion object {
        private lateinit var mesh: Mesh
        private var uniformMatTranslate = -1
        private var uniformMatTransform = -1
        private var uniformProjection = -1
        private var uniformTextTexture = -1
        private var uniformColor = -1
        private var uniformGlobalTransform = -1
        private var uniformDimension = -1

        fun updateProjection(matrix: Mat4) {
            mesh.program.setMatrix(uniformProjection, matrix)
        }

        fun updateGlobalTransform(matrix: Mat4) {
            mesh.program.setMatrix(uniformGlobalTransform, matrix)
        }

        fun initialize() {
            mesh = Mesh()
            mesh.addElement(VertexElement("position", 0, 2))
            mesh.addElement(VertexElement("texCoord", 0, 2))

            val program = Program()
            program.compileFragmentShaderFromResource(Shader.TEXT.fragmentShader)
            program.compileVertexShaderFromResource(Shader.TEXT.vertexShader)
            program.link()

            uniformMatTranslate = program.getUniform("matTranslate")
            uniformMatTransform = program.getUniform("matTransform")
            uniformProjection = program.getUniform("matProjection")
            uniformTextTexture = program.getUniform("textTexture")
            uniformColor = program.getUniform("color")
            uniformGlobalTransform = program.getUniform("globalTransform")
            uniformDimension = program.getUniform("dimension")

            mesh.program = program
            mesh.blendMode = BlendMode.ALPHA

            val vb = VertexBuffer()
            val vbData = BufferUtils.createFloatBuffer(4 * 4)
            vbData.put(0.0f).put(1.0f).put(0.0f).put(1.0f)
            vbData.put(1.0f).put(1.0f).put(1.0f).put(1.0f)
            vbData.put(1.0f).put(0.0f).put(1.0f).put(0.0f)
            vbData.put(0.0f).put(0.0f).put(0.0f).put(0.0f)
            vbData.flip()
            vb.data(vbData)

            val ib = IndexBuffer(IndexType.UINT8)
            val ibData = BufferUtils.createByteBuffer(6)
            ibData.put(0).put(1).put(2)
            ibData.put(0).put(2).put(3)
            ibData.flip()
            ib.data(ibData)

            mesh.vertexBuffer = vb
            mesh.indexBuffer = ib
            mesh.indexCount = 6

            mesh.finalize()
        }
    }
}