package ch.cromon.wildstar.ui

import ch.cromon.wildstar.gx.GxContext
import ch.cromon.wildstar.io.ImageSource
import ch.cromon.wildstar.ui.messaging.*
import ch.cromon.wildstar.ui.utils.FpsCounter
import glm.vec2.Vec2
import org.lwjgl.BufferUtils
import org.lwjgl.glfw.GLFW.*
import org.lwjgl.glfw.GLFWImage
import org.lwjgl.opengl.GL
import org.lwjgl.system.MemoryUtil.NULL
import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.util.*
import javax.annotation.PostConstruct


@Component
class Window @Autowired constructor(private val context: GxContext) {
    companion object {
        private val buildProperties = Properties()
        private val BUILD_VERSION_KEY = "gradle.build.version"
        private val BUILD_TIME_KEY = "gradle.build.time"
        private val GIT_HASH_KEY = "git.hash"

        init {
            buildProperties.load(Window::class.java.getResourceAsStream("/build.properties"))
        }
    }

    @Autowired
    private lateinit var logger: Logger

    private var window: Long = 0

    @Autowired
    private lateinit var uiManager: UiManager

    @PostConstruct
    fun initialize() {
        logger.info("Initializing window subsystem...")

        glfwInit()
        window = glfwCreateWindow(1024, 768, getWindowTitle(), NULL, NULL)
        glfwSetWindowIcon(window, getWindowIcons())

        glfwSwapInterval(1)
        glfwSetWindowSizeCallback(window, { _, w, h ->
            reshape(w, h)
        })

        init()
    }

    fun runLoop() {
        while (!glfwWindowShouldClose(window)) {
            glfwPollEvents()
            FpsCounter.onFrame()
            uiManager.updateFps(FpsCounter.fps)
            context.beginFrame()
            uiManager.onFrame()
            glfwSwapBuffers(window)
        }

        glfwHideWindow(window)
    }

    fun show() {
        glfwShowWindow(window)
    }

    private fun reshape(width: Int, height: Int) {
        context.onResize(width, height)
        uiManager.onResize(width, height)
    }

    private fun init() {
        glfwMakeContextCurrent(window)
        GL.createCapabilities()
        context.onInitialize()
        uiManager.onInitialize()
        loadEventHandlers()

        val w = intArrayOf(0)
        val h = intArrayOf(0)
        glfwGetWindowSize(window, w, h)
        reshape(w[0], h[0])
    }

    private fun loadEventHandlers() {
        glfwSetCursorPosCallback(window, { _, x, y ->
            onMouseMove(x, y)
        })

        glfwSetMouseButtonCallback(window, { _, button, action, _ ->
            onMouseClickEvent(button, action == GLFW_PRESS)
        })

        glfwSetCharCallback(window, { _, codePoint ->
            onKeyEvent(codePoint.toChar(), true)
        })

        glfwSetKeyCallback(window, { _, key, _, action, _ ->
            if (action != GLFW_REPEAT) {
                onKeyEvent(key, action == GLFW_PRESS)
            }
        })

        glfwSetScrollCallback(window, { _, dx, dy ->
            onScroll(dx.toFloat(), dy.toFloat())
        })
    }

    private fun onMouseMove(x: Double, y: Double) {
        //mouse.position = Vec2(x, y)
        val message = MouseMoveMessage(Vec2(x, y), uiManager.context)
        uiManager.handleMessage(message)
    }

    private fun onMouseClickEvent(buttonRaw: Int, isPressed: Boolean) {
        val button = when (buttonRaw) {
            GLFW_MOUSE_BUTTON_1 -> MouseButton.LEFT
            GLFW_MOUSE_BUTTON_2 -> MouseButton.RIGHT
            GLFW_MOUSE_BUTTON_3 -> MouseButton.MIDDLE
            else -> MouseButton.UNKNOWN
        }

        val x = doubleArrayOf(0.0)
        val y = doubleArrayOf(0.0)
        glfwGetCursorPos(window, x, y)
        //mouse.setButton(button, isPressed)

        val message = MouseClickMessage(Vec2(x[0], y[0]), button, isPressed, uiManager.context)
        uiManager.handleMessage(message)
    }

    private fun onKeyEvent(scanCode: Int, pressed: Boolean) {
        if (uiManager.context.inputFocus == null) {
            //keyBoard.setKeyState(scanCode.toShort(), pressed)
        }

        if (pressed) {
            uiManager.handleMessage(KeyPressMessage('\u0000', scanCode.toShort(), uiManager.context))
        } else {
            uiManager.handleMessage(KeyReleaseMessage('\u0000', scanCode.toShort(), uiManager.context))
        }
    }

    private fun onKeyEvent(keyChar: Char, pressed: Boolean) {
        if (pressed) {
            uiManager.handleMessage(KeyPressMessage(keyChar, 0, uiManager.context))
        } else {
            uiManager.handleMessage(KeyReleaseMessage(keyChar, 0, uiManager.context))
        }
    }

    private fun onScroll(dx: Float, dy: Float) {
        val x = doubleArrayOf(0.0)
        val y = doubleArrayOf(0.0)
        glfwGetCursorPos(window, x, y)
        uiManager.handleMessage(MouseWheelMessage(Vec2(x[0], y[0]), dx, dy, uiManager.context))
    }

    private fun getWindowTitle(): String {
        val version = buildProperties.getProperty(BUILD_VERSION_KEY)
        val buildTime = buildProperties.getProperty(BUILD_TIME_KEY)
        val gitHash = buildProperties.getProperty(GIT_HASH_KEY)

        return "Wildstar-Studio -- Version: $version, Build: $buildTime, Commit: $gitHash"
    }

    private fun getWindowIcons(): GLFWImage.Buffer {
        val ret = GLFWImage.calloc(1)
        val image = GLFWImage.calloc()
        ImageSource.fromImageResource("application_icon.png").run {
            val nativeBuffer = BufferUtils.createByteBuffer(buffer.size)
            nativeBuffer.put(buffer).flip()
            image.set(width, height, nativeBuffer)
        }
        ret.put(0, image)
        return ret
    }
}