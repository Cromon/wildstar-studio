package ch.cromon.wildstar.io

import java.awt.image.BufferedImage
import java.awt.image.DataBuffer
import java.awt.image.DataBufferByte
import java.awt.image.DataBufferInt
import javax.imageio.ImageIO


class ImageSource(val buffer: ByteArray, val width: Int, val height: Int) {
    companion object {
        fun fromImageResource(name: String): ImageSource {
            return ImageSource::class.java.getResourceAsStream("/images/$name")?.use {
                var img = ImageIO.read(it)
                if(img.type != BufferedImage.TYPE_INT_ARGB) {
                    val tmp = BufferedImage(img.width, img.height, BufferedImage.TYPE_INT_ARGB)
                    val graphics = tmp.createGraphics()
                    graphics.drawImage(img, 0, 0, img.width, img.height, null)
                    img = tmp
                }
                fromBufferedImage(img)
            } ?: throw IllegalArgumentException("Unable to find resource images/$name")
        }

        fun fromBufferedImage(image: BufferedImage): ImageSource {
            val dataBuffer = image.raster.dataBuffer
            when {
                dataBuffer.dataType == DataBuffer.TYPE_BYTE -> return ImageSource((dataBuffer as DataBufferByte).data, image.width, image.height)
                dataBuffer.dataType == DataBuffer.TYPE_INT -> {
                    val intBuffer = dataBuffer as DataBufferInt
                    val raw = intBuffer.data
                    val data = ByteArray(image.width * image.height * 4)
                    for(i in 0 until raw.size) {
                        val color = raw[i]
                        data[i * 4] = (color and 0xFF).toByte()
                        data[i * 4 + 1] = ((color shr 8) and 0xFF).toByte()
                        data[i * 4 + 2] = ((color shr 16) and 0xFF).toByte()
                        data[i * 4 + 3] = ((color shr 24) and 0xFF).toByte()
                    }

                    return ImageSource(data, image.width, image.height)
                }
                else -> throw IllegalArgumentException("BufferedImage must be backed by an IntBuffer or ByteBuffer")
            }
        }
    }
}