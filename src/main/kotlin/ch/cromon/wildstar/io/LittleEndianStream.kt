package ch.cromon.wildstar.io

import java.io.EOFException
import java.io.InputStream


class LittleEndianStream(val data: ByteArray) : InputStream() {
    override fun read(): Int {
        if(position >= size) {
            throw EOFException("Cannot read past the end of the stream")
        }

        return this.data[position++].toInt()
    }

    var position = 0
    val size = data.size

    fun readLong(): Long {
        val b0 = data[position++].toLong() and 0xFF
        val b1 = data[position++].toLong() and 0xFF
        val b2 = data[position++].toLong() and 0xFF
        val b3 = data[position++].toLong() and 0xFF
        val b4 = data[position++].toLong() and 0xFF
        val b5 = data[position++].toLong() and 0xFF
        val b6 = data[position++].toLong() and 0xFF
        val b7 = data[position++].toLong() and 0xFF

        return b0 or (b1 shl 8) or (b2 shl 16) or (b3 shl 24) or (b4 shl 32) or (b5 shl 40) or (b6 shl 48) or (b7 shl 56)
    }

    fun readInt(): Int {
        val b0 = data[position++].toInt() and 0xFF
        val b1 = data[position++].toInt() and 0xFF
        val b2 = data[position++].toInt() and 0xFF
        val b3 = data[position++].toInt() and 0xFF

        return b0 or (b1 shl 8) or (b2 shl 16) or (b3 shl 24)
    }

    fun readShort(): Short {
        val b0 = data[position++].toInt()
        val b1 = data[position++].toInt()

        return (b0 or (b1 shl 8)).toShort()
    }

    fun readByte() = data[position++]

    fun readBytes(numBytes: Int): ByteArray {
        val ret = data.copyOfRange(position, position + numBytes)
        position += numBytes
        return ret
    }
}