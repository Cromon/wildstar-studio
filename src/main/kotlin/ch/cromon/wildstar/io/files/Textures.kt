package ch.cromon.wildstar.io.files

import ch.cromon.wildstar.gx.Texture
import ch.cromon.wildstar.io.LittleEndianStream
import ch.cromon.wildstar.io.files.DataFormat.*
import org.apache.commons.io.IOUtils
import org.lwjgl.opengl.EXTTextureCompressionS3TC.*
import org.lwjgl.opengl.GL11.GL_RED
import org.lwjgl.opengl.GL11.GL_RGBA
import org.slf4j.LoggerFactory
import java.io.FileOutputStream

private enum class DataFormat(val native: Int) {
    RGBA(GL_RGBA),
    R(GL_RED),
    DXT1(GL_COMPRESSED_RGB_S3TC_DXT1_EXT),
    DXT3(GL_COMPRESSED_RGBA_S3TC_DXT3_EXT),
    DXT5(GL_COMPRESSED_RGBA_S3TC_DXT5_EXT),
    UNKNOWN(-1)
}

private class TexFormatEntry(val blockSize: Int, val format: DataFormat, val compressed: Boolean)

/****************************************
 *
 * val0: if(val0 > 1) --> v16 = width / val0
 * val1
 * val2
 * val3: if(val3 > 1) --> v20 = height / val3
 * val4:
 * val5:
 * val6: if(val6 > 1) --> v23 = depth / val6
 *
 */

private val formatTable = arrayOf(
        TexFormatEntry(8, DXT1, true),
        TexFormatEntry(16, DXT3, true),
        TexFormatEntry(16, DXT5, true),
        TexFormatEntry(4, RGBA, false),
        TexFormatEntry(4, RGBA, false),
        TexFormatEntry(2, UNKNOWN, false),
        TexFormatEntry(2, UNKNOWN, false),
        TexFormatEntry(2, UNKNOWN, false),
        TexFormatEntry(2, UNKNOWN, false),
        TexFormatEntry(1, R, false),
        TexFormatEntry(2, UNKNOWN, false),
        TexFormatEntry(4, UNKNOWN, false),
        TexFormatEntry(8, UNKNOWN, false),
        TexFormatEntry(2, UNKNOWN, false),
        TexFormatEntry(4, UNKNOWN, false),
        TexFormatEntry(4, UNKNOWN, false),
        TexFormatEntry(2, UNKNOWN, false),
        TexFormatEntry(4, UNKNOWN, false),
        TexFormatEntry(8, UNKNOWN, false),
        TexFormatEntry(4, UNKNOWN, false),
        TexFormatEntry(16, UNKNOWN, false),
        TexFormatEntry(2, UNKNOWN, false),
        TexFormatEntry(4, UNKNOWN, false),
        TexFormatEntry(4, UNKNOWN, false),
        TexFormatEntry(0, UNKNOWN, false),
        TexFormatEntry(4, UNKNOWN, false),
        TexFormatEntry(4, UNKNOWN, false)
)

private class TexHeader(stream: LittleEndianStream) {
    val width: Int
    val height: Int
    val mipCount: Int
    val formatIndex: Int
    val hasSizesData: Int
    val sizes = IntArray(14)

    init {
        val signature = stream.readInt()
        stream.readInt()
        if(signature != 0x00474658) {
            throw IllegalArgumentException("Unknown texture file, signature was 0x${signature.toString(16)}, expected 0x00474658")
        }

        width = stream.readInt()
        height = stream.readInt()
        stream.readInt()
        stream.readInt()
        mipCount = stream.readInt()
        formatIndex = stream.readInt()
        hasSizesData = stream.readInt()
        stream.readInt()
        stream.readInt()
        stream.readInt()
        stream.readInt()
        stream.readInt()
        if(hasSizesData != 0) {
            for (i in 0 until 14) {
                sizes[i] = stream.readInt()
            }
        } else {
            stream.position += 14 * 4
        }
    }
}

private class TextureLayer(val width: Int, val height: Int, val data: ByteArray)

class UnpackedImage(val width: Int, val height: Int, val data: ByteArray)

class TextureFile(private val content: LittleEndianStream) {
    companion object {
        private val LOG = LoggerFactory.getLogger(TextureFile::class.java)

        private val tmpColorArray = arrayOf(
                byteArrayOf(0, 0, 0, 255.toByte()),
                byteArrayOf(0, 0, 0, 255.toByte()),
                byteArrayOf(0, 0, 0, 255.toByte()),
                byteArrayOf(0, 0, 0, 255.toByte())
        )

        private val tableIndices = ShortArray(16)
        private val alphaValues = ByteArray(16)
        private val alphaLookup = ShortArray(16)

        private fun rgb565ToRgb8Array(input: Short, output: ByteArray) {
            var r = (input.toInt() and 0x1F)
            var g = ((input.toInt() shr 5) and 0x3F)
            var b = ((input.toInt() shr 11) and 0x3F)

            r = (r shl 3) or (r shr 2)
            g = (g shl 2) or (g shr 4)
            b = (b shl 3) or (b shr 2)

            output[0] = r.toByte()
            output[1] = g.toByte()
            output[2] = b.toByte()
        }
    }

    private lateinit var formatEntry: TexFormatEntry
    private val layers = ArrayList<TextureLayer>()

    fun parse() {
        FileOutputStream("texture.tex").use {
            IOUtils.write(content.data, it)
        }

        content.position = 0
        val header = TexHeader(content)
        if(header.formatIndex < 0 || header.formatIndex >= formatTable.size) {
            LOG.warn("Texture has format index {} which is outside of the valid bounds [0,{})", header.formatIndex, formatTable.size)
            throw IllegalArgumentException("Stream does not contain a valid texture")
        }

        formatEntry = formatTable[header.formatIndex]
        if(formatEntry.format == UNKNOWN) {
            LOG.warn("Texture has format index {} which is not yet parsable.", header.formatIndex)
            throw IllegalArgumentException("Unknown texture type")
        }

        for((index, mip) in ((header.mipCount - 1) downTo 0).withIndex()) {
            val w = Math.max(1, header.width shr mip)
            val h = Math.max(1, header.height shr mip)

            val layerData = content.readBytes(header.sizes[index])
            layers.add(TextureLayer(w, h, layerData))
        }

        layers.reverse()
    }

    fun loadToTexture(texture: Texture) {
        for((i, layer) in layers.withIndex()) {
            texture.loadLayer(i, layer.width, layer.height, formatEntry.format.native, formatEntry.compressed, layer.data)
        }
    }

    fun unpackColors(): UnpackedImage {
        return when(formatEntry.format) {
            RGBA -> unpackRgba()
            R -> unpackOneChannel()
            DXT1 -> unpackDxt(this::dxt1GetBlock)
            DXT3 -> unpackDxt(this::dxt3GetBlock)
            DXT5 -> unpackDxt(this::dxt5GetBlock)
            UNKNOWN -> throw IllegalStateException("Cannot convert unknown texture")
        }
    }

    private fun unpackRgba() = UnpackedImage(layers[0].width, layers[0].height, layers[0].data)

    private fun unpackOneChannel(): UnpackedImage {
        val layer = layers[0]
        val w = layer.width
        val h = layer.height
        val data = ByteArray(w * h * 4)
        for(i in 0 until w * h) {
            data[i * 4] = layer.data[i]
            data[i * 4 + 1] = 0
            data[i * 4 + 2] = 0
            data[i * 4 + 3] = 255.toByte()
        }

        return UnpackedImage(w, h, data)
    }

    private fun unpackDxt(callback: (LittleEndianStream, ByteArray, Int) -> Unit): UnpackedImage {
        val layer = layers[0]
        val numBlocksFull = (Math.ceil(layer.width / 4.0) * Math.ceil(layer.height / 4.0)).toInt()
        val blockData = ByteArray(numBlocksFull * 16 * 4)
        val stream = LittleEndianStream(layer.data)
        for(i in 0 until numBlocksFull) {
            callback(stream, blockData, i * 16 * 4)
        }

        val colorData = ByteArray(layer.width * layer.height * 4)
        for(y in 0 until layer.height) {
            for(x in 0 until layer.width) {
                val bx = x / 4
                val by = y / 4

                val ibx = x % 4
                val iby = y % 4

                val blockIndex = by * Math.ceil(layer.width / 4.0).toInt() + bx
                val innerIndex = iby * 4 + ibx
                val outIndex = y * layer.width + x
                colorData[outIndex * 4] = blockData[blockIndex * 16 * 4 + innerIndex * 4]
                colorData[outIndex * 4 + 1] = blockData[blockIndex * 16 * 4 + innerIndex * 4 + 1]
                colorData[outIndex * 4 + 2] = blockData[blockIndex * 16 * 4 + innerIndex * 4 + 2]
                colorData[outIndex * 4 + 3] = blockData[blockIndex * 16 * 4 + innerIndex * 4 + 3]
            }
        }

        return UnpackedImage(layer.width, layer.height, colorData)
    }

    private fun dxt1GetBlock(stream: LittleEndianStream, colors: ByteArray, baseIndex: Int) {
        val color1 = stream.readShort()
        val color2 = stream.readShort()

        rgb565ToRgb8Array(color1, tmpColorArray[0])
        rgb565ToRgb8Array(color2, tmpColorArray[1])

        val clr1 = tmpColorArray[0]
        val clr2 = tmpColorArray[1]
        val clr3 = tmpColorArray[2]
        val clr4 = tmpColorArray[3]

        if(color1 > color2) {
            for(i in 0 until 3) {
                clr4[i] = ((clr1[i] + 2 * clr2[i]) / 3).toByte()
                clr3[i] = ((2 * clr1[i] + clr2[i]) / 3).toByte()
            }
        } else {
            for(i in 0 until 3) {
                clr3[i] = ((clr1[i] + clr2[i]) / 2).toByte()
                clr4[i] = 0
            }
        }

        val indices = stream.readInt()
        for(i in 0 until 16) {
            tableIndices[i] = ((indices shr (2 * i)) and 3).toShort()
        }

        for(y in 0 until 4) {
            for(x in 0 until 4) {
                val index = y * 4 + x
                val clr = when(tableIndices[index].toInt()) {
                    0 -> clr1
                    1 -> clr2
                    2 -> clr3
                    3 -> clr4
                    else -> clr1
                }
                colors[baseIndex + index * 4] = clr[0]
                colors[baseIndex + index * 4 + 1] = clr[1]
                colors[baseIndex + index * 4 + 2] = clr[2]
                colors[baseIndex + index * 4 + 3] = 255.toByte()
            }
        }
    }

    private fun dxt3GetBlock(stream: LittleEndianStream, colors: ByteArray, baseIndex: Int) {
        val alpha = stream.readLong()
        for(i in 0 until 16) {
            alphaValues[i] = ((((alpha shr (4 * i)) and 0x0F) / 15.0f) * 255.0f).toByte()
        }

        val color1 = stream.readShort()
        val color2 = stream.readShort()

        rgb565ToRgb8Array(color1, tmpColorArray[0])
        rgb565ToRgb8Array(color2, tmpColorArray[1])

        val clr1 = tmpColorArray[0]
        val clr2 = tmpColorArray[1]
        val clr3 = tmpColorArray[2]
        val clr4 = tmpColorArray[3]

        if(color1 > color2) {
            for(i in 0 until 3) {
                clr4[i] = ((clr1[i] + 2 * clr2[i]) / 3).toByte()
                clr3[i] = ((2 * clr1[i] + clr2[i]) / 3).toByte()
            }
        } else {
            for(i in 0 until 3) {
                clr3[i] = ((clr1[i] + clr2[i]) / 2).toByte()
                clr4[i] = 0
            }
        }

        val indices = stream.readInt()
        for(i in 0 until 16) {
            tableIndices[i] = ((indices shr (2 * i)) and 3).toShort()
        }

        for(y in 0 until 4) {
            for(x in 0 until 4) {
                val index = y * 4 + x
                val clr = when(tableIndices[index].toInt()) {
                    0 -> clr1
                    1 -> clr2
                    2 -> clr3
                    3 -> clr4
                    else -> clr1
                }
                colors[baseIndex + index * 4] = clr[0]
                colors[baseIndex + index * 4 + 1] = clr[1]
                colors[baseIndex + index * 4 + 2] = clr[2]
                colors[baseIndex + index * 4 + 3] = alphaValues[index]
            }
        }
    }

    private fun dxt5GetBlock(stream: LittleEndianStream, colors: ByteArray, baseIndex: Int) {
        val alpha1 = stream.readByte()
        val alpha2 = stream.readByte()

        alphaValues[0] = alpha1
        alphaValues[1] = alpha2

        if(alpha1 > alpha2) {
            for(i in 0 until 6) {
                alphaValues[i + 2] = (((6.0f - i) * alpha1 + (1.0f + i) * alpha2) / 7.0f).toByte()
            }
        } else {
            for(i in 0 until 4) {
                alphaValues[i + 2] = (((4.0f - i) * alpha1 + (1.0f + i) * alpha2) / 5.0f).toByte()
            }

            alphaValues[6] = 0
            alphaValues[7]= 255.toByte()
        }

        val lu1 = stream.readInt()
        val lu2 = stream.readShort()
        val lookupValue = lu1.toLong() or (lu2.toLong() shl 32)
        for(i in 0 until 16) {
            alphaLookup[i] = ((lookupValue shr (i * 3)) and 7).toShort()
        }

        val color1 = stream.readShort()
        val color2 = stream.readShort()

        rgb565ToRgb8Array(color1, tmpColorArray[0])
        rgb565ToRgb8Array(color2, tmpColorArray[1])

        val clr1 = tmpColorArray[0]
        val clr2 = tmpColorArray[1]
        val clr3 = tmpColorArray[2]
        val clr4 = tmpColorArray[3]

        if(color1 > color2) {
            for(i in 0 until 3) {
                clr4[i] = ((clr1[i] + 2 * clr2[i]) / 3).toByte()
                clr3[i] = ((2 * clr1[i] + clr2[i]) / 3).toByte()
            }
        } else {
            for(i in 0 until 3) {
                clr3[i] = ((clr1[i] + clr2[i]) / 2).toByte()
                clr4[i] = 0
            }
        }

        val indices = stream.readInt()
        for(i in 0 until 16) {
            tableIndices[i] = ((indices shr (2 * i)) and 3).toShort()
        }

        for(y in 0 until 4) {
            for(x in 0 until 4) {
                val index = y * 4 + x
                val clr = when(tableIndices[index].toInt()) {
                    0 -> clr1
                    1 -> clr2
                    2 -> clr3
                    3 -> clr4
                    else -> clr1
                }
                colors[baseIndex + index * 4] = clr[0]
                colors[baseIndex + index * 4 + 1] = clr[1]
                colors[baseIndex + index * 4 + 2] = clr[2]
                colors[baseIndex + index * 4 + 3] = alphaValues[alphaLookup[index].toInt()]
            }
        }
    }
}