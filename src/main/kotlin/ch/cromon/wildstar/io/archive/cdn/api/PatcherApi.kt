package ch.cromon.wildstar.io.archive.cdn.api

import ch.cromon.wildstar.io.archive.cdn.CdnFileManager
import com.sun.org.apache.xerces.internal.impl.dv.util.HexBin
import feign.Feign
import feign.Param
import feign.RequestLine
import feign.slf4j.Slf4jLogger
import org.springframework.stereotype.Component


interface Patcher {
    @RequestLine("GET /version.txt")
    fun version(): String

    @RequestLine("GET /{version}/Patch.index.bin")
    fun indexFileHash(@Param("version") version: Int): ByteArray

    @RequestLine("GET /{version}/{hash}")
    fun fileContent(@Param("version") version: Int, @Param("hash") hash: String): ByteArray
}

@Component
class PatchApi {
    private val patcher = Feign.builder()
            .logger(Slf4jLogger(PatchApi::class.java))
            .logLevel(feign.Logger.Level.BASIC)
            .target(Patcher::class.java, CdnFileManager.PATCH_VERSION_URL)

    fun getVersion(): Int = patcher.version().toInt()

    fun getIndexFileHash(version: Int) = HexBin.encode(patcher.indexFileHash(version)).toLowerCase()

    fun getFile(version: Int, hash: String) = patcher.fileContent(version, hash)
}