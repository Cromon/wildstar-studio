package ch.cromon.wildstar.io.archive.filesystem

import SevenZip.Compression.LZMA.Decoder
import ch.cromon.wildstar.io.LittleEndianStream
import ch.cromon.wildstar.io.archive.FileEntry
import ch.cromon.wildstar.io.archive.PackDirectoryHeader
import ch.cromon.wildstar.io.archive.cdn.ArchiveLoadEvent
import ch.cromon.wildstar.utils.readIntLE
import ch.cromon.wildstar.utils.readLongLE
import org.apache.commons.io.IOUtils
import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.RandomAccessFile
import java.util.*
import java.util.zip.Deflater
import java.util.zip.DeflaterInputStream
import javax.annotation.PreDestroy

class AarcEntry(val blockIndex: Int, val hash: ByteArray)

@Component
@Scope("prototype")
class ArchiveParser {
    companion object {
        private val FILE_SIGNATURE = 0x5041434B
        private val AARC_SIGNATURE = 0x41415243
    }

    @Autowired
    private lateinit var logger: Logger

    @Autowired
    private lateinit var publisher: ApplicationEventPublisher

    private val directoryList = ArrayList<PackDirectoryHeader>()
    private val aarcTable = ArrayList<AarcEntry>()

    private lateinit var file: RandomAccessFile

    @PreDestroy
    fun cleanup() {
        try {
            file.close()
        } catch (e: UninitializedPropertyAccessException) {

        }
    }

    fun parse(file: RandomAccessFile) {
        this.file = file
        val signature = file.readIntLE()
        if (signature != FILE_SIGNATURE) {
            logger.warn("Invalid file signature in archive: Got $signature expected $FILE_SIGNATURE")
            throw IllegalArgumentException("Invalid file signature")
        }
        val version = file.readIntLE()
        if (version != 1) {
            logger.warn("Invalid archive version in archive: Got $version expected 1")
            throw IllegalArgumentException("Invalid file version")
        }

        updateStatus("Parsing directory table")

        file.skipBytes(528)
        val pkDirStart = file.readLongLE()
        val pkDirCount = file.readIntLE()

        file.seek(pkDirStart)

        for (i in 0 until pkDirCount) {
            directoryList.add(PackDirectoryHeader(file.readLongLE(), file.readLongLE()))
        }

        updateStatus("Seeking AARC block")
        var aarcEntryCount = 0
        var aarcEntryBlock = -1
        for (block in directoryList) {
            if (block.blockSize != 16.toLong()) {
                continue
            }

            file.seek(block.offset)
            if (file.readIntLE() != AARC_SIGNATURE) {
                continue
            }

            file.readIntLE()
            aarcEntryCount = file.readIntLE()
            aarcEntryBlock = file.readIntLE()
        }

        if (aarcEntryBlock < 0) {
            throw IllegalArgumentException("No AARC entry found in archive")
        }

        updateStatus("Parsing AARC table")
        val block = directoryList[aarcEntryBlock]
        file.seek(block.offset)
        for (i in 0 until aarcEntryCount) {
            val hash = ByteArray(20)
            val blockIndex = file.readIntLE()
            file.readFully(hash)
            file.readLongLE()
            aarcTable.add(AarcEntry(blockIndex, hash))
        }
    }

    fun findFile(fileEntry: FileEntry): LittleEndianStream? {
        for(entry in aarcTable) {
            if(Arrays.equals(entry.hash, fileEntry.hash)) {
                val block = directoryList[entry.blockIndex]
                file.seek(block.offset)
                val content = ByteArray(block.blockSize.toInt())
                file.readFully(content)
                return when {
                    (fileEntry.flags and 2) != 0 -> {
                        val deflate = Deflater()
                        val input = ByteArrayInputStream(content)
                        DeflaterInputStream(input, deflate).use {
                            val os = ByteArrayOutputStream()
                            IOUtils.copy(input, os)
                            LittleEndianStream(os.toByteArray())
                        }
                    }
                    (fileEntry.flags and 4) != 0 -> {
                        val os = ByteArrayOutputStream()

                        ByteArrayInputStream(content).use {
                            val decoder = Decoder()
                            val properties = ByteArray(5)
                            IOUtils.readFully(it, properties)
                            decoder.SetDecoderProperties(properties)
                            decoder.Code(it, os, fileEntry.uncompressedSize)
                        }
                        LittleEndianStream(os.toByteArray())
                    }
                    else -> LittleEndianStream(content)
                }
            }
        }

        return null
    }

    private fun updateStatus(status: String) {
        publisher.publishEvent(ArchiveLoadEvent(this, status))
    }
}