package ch.cromon.wildstar.io.archive.cdn

import SevenZip.Compression.LZMA.Decoder
import ch.cromon.wildstar.io.LittleEndianStream
import ch.cromon.wildstar.io.archive.DirectoryEntry
import ch.cromon.wildstar.io.archive.FileEntry
import ch.cromon.wildstar.io.archive.FileHandler
import ch.cromon.wildstar.io.archive.IndexParser
import ch.cromon.wildstar.io.archive.cdn.api.PatchApi
import com.sun.org.apache.xerces.internal.impl.dv.util.HexBin
import org.apache.commons.io.IOUtils
import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationEvent
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Component
import java.io.*
import java.util.concurrent.CompletableFuture
import java.util.zip.Deflater
import java.util.zip.DeflaterInputStream

class ArchiveLoadEvent(source: Any, val message: String): ApplicationEvent(source)

@Component
@Qualifier("CDN")
class CdnFileManager : FileHandler() {
    companion object {
        val PATCH_VERSION_URL = "http://wildstar.patcher.ncsoft.com/"
    }

    @Autowired
    private lateinit var publisher: ApplicationEventPublisher

    @Autowired
    private lateinit var context: ApplicationContext

    @Autowired
    private lateinit var logger: Logger

    @Autowired
    private lateinit var patcher: PatchApi

    @Autowired
    private lateinit var indexParser: IndexParser

    private var remoteVersion = 0

    override fun getFileRoot(): DirectoryEntry {
        return indexParser.fileRoot
    }

    override fun initialize(file: File?): CompletableFuture<Void> {
        logger.info("Initializing CDN manager")
        return CompletableFuture.runAsync {
            val downloadData = checkVersion()
            if(downloadData) {
                downloadIndex()
            }
        }
    }

    override fun loadFile(entry: FileEntry): CompletableFuture<LittleEndianStream> {
        return CompletableFuture.supplyAsync({
            val content = patcher.getFile(remoteVersion, "${HexBin.encode(entry.hash).toLowerCase()}.bin")
            when {
                (entry.flags and 2) != 0 -> {
                    val deflate = Deflater()
                    val input = ByteArrayInputStream(content)
                    DeflaterInputStream(input, deflate).use {
                        val os = ByteArrayOutputStream()
                        IOUtils.copy(input, os)
                        LittleEndianStream(os.toByteArray())
                    }
                }
                (entry.flags and 4) != 0 -> {
                    val os = ByteArrayOutputStream()

                    ByteArrayInputStream(content).use {
                        val decoder = Decoder()
                        val properties = ByteArray(5)
                        IOUtils.readFully(it, properties)
                        decoder.SetDecoderProperties(properties)
                        decoder.Code(it, os, entry.uncompressedSize)
                    }
                    LittleEndianStream(os.toByteArray())
                }
                else -> LittleEndianStream(content)
            }
        })
    }

    private fun checkVersion(): Boolean {
        updateStatus("Contacting patch server")
        logger.info("Fetching versions from $PATCH_VERSION_URL")
        remoteVersion = patcher.getVersion()
        logger.info("Remote version: $remoteVersion")

        try {
            FileInputStream("cache/ClientData.index").use {
                val os = ByteArrayOutputStream()
                IOUtils.copy(it, os)
                val stream = LittleEndianStream(os.toByteArray())
                val localHeader = IndexParser.parseHeader(stream)
                stream.position = 0
                if(localHeader.patch == remoteVersion) {
                    logger.info("Local cache with same version found at ${File("cache", "ClientData.index").absolutePath}")
                    if(loadFromLocal(stream)) {
                        return false
                    }
                }
                return true
            }
        } catch (e: FileNotFoundException) {
            return true
        }
    }

    private fun loadFromLocal(strm: LittleEndianStream): Boolean {
        try {
            indexParser.load(strm)
            indexParser.loadIndexTree()
        } catch (e: Throwable) {
            return false
        }
        return true
    }

    private fun downloadIndex() {
        updateStatus("Fetching Patch.index.bin")
        val patchHash = patcher.getIndexFileHash(remoteVersion)
        val patchFile = patcher.getFile(remoteVersion, "$patchHash.bin")
        indexParser.load(LittleEndianStream(patchFile))
        indexParser.loadIndexTree()

        val indexHash = HexBin.encode(indexParser.getFile("ClientData.index")?.hash ?: throw IllegalStateException("No ClientData.index contained in patch")).toLowerCase()
        updateStatus("Fetching ClientData.index")


        indexParser = context.getBean(IndexParser::class.java)
        val indexData = patcher.getFile(remoteVersion, "$indexHash.bin")
        writeCache(indexData)
        updateStatus("Parsing ClientData.index")
        indexParser.load(LittleEndianStream(indexData))
        indexParser.loadIndexTree()
    }

    private fun updateStatus(status: String) {
        publisher.publishEvent(ArchiveLoadEvent(this, status))
    }

    private fun writeCache(data: ByteArray) {
        val file = File("./cache")
        if(!file.exists()) {
            file.mkdirs()
        }

        FileOutputStream(File(file, "ClientData.index")).use {
            IOUtils.write(data, it)
        }
    }
}