package ch.cromon.wildstar.io.archive

import ch.cromon.wildstar.io.LittleEndianStream
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component
import java.io.File
import java.nio.charset.StandardCharsets
import java.util.*
import java.util.Collections.unmodifiableList
import java.util.concurrent.CompletableFuture


class ArchiveIndexInfo(val magic: Int, val version: Int, val patch: Int, val rootBlock: Int)
class PackDirectoryHeader(val offset: Long, val blockSize: Long)

abstract class FileSystemEntry(private val parent: FileSystemEntry?, internal val name: String) {
    open fun hasChildren(): Boolean = false

    fun getFullPath(): String {
        var path = name
        var curEntry = parent
        while (curEntry != null) {
            if (curEntry.name.isNotBlank()) {
                path = "${curEntry.name}\\$path"
            }

            curEntry = curEntry.parent
        }

        return path
    }
}

class DirectoryEntry(parent: FileSystemEntry?, name: String, private val nextBlock: Int, private val index: IndexParser) : FileSystemEntry(parent, name) {
    var isRoot = false

    val children = ArrayList<FileSystemEntry>()
    override fun hasChildren(): Boolean = children.size > 0

    fun getFiles(): List<FileSystemEntry> {
        val ret = ArrayList<FileSystemEntry>()
        for (entry in children) {
            if (entry is DirectoryEntry) {
                ret.addAll(entry.getFiles())
            } else {
                ret.add(entry)
            }
        }

        return unmodifiableList(ret)
    }

    fun getEntries(): List<FileSystemEntry> {
        return unmodifiableList(children.map { (it as? DirectoryEntry)?.getEntries() ?: listOf(it) }.flatten())
    }

    fun getFile(path: String): FileSystemEntry? {
        val idx = path.indexOf('\\')
        if (idx < 0) {
            return children.find { it.name.toUpperCase() == path.toUpperCase() }
        }

        val dir = path.substring(0, idx)
        val remain = path.substring(idx + 1)
        return children
                .filter({ it is DirectoryEntry })
                .map({ it as DirectoryEntry })
                .find { it.name.toUpperCase() == dir.toUpperCase() }?.getFile(remain)
    }

    fun parseChildren() {
        val nextBlock = index.getDirectoryHeader(nextBlock)
        val stream = index.stream
        stream.position = nextBlock.offset.toInt()

        val numDirectories = stream.readInt()
        val numFiles = stream.readInt()

        val curPos = stream.position
        val dataSize = numDirectories * 8 + numFiles * 56
        stream.position += dataSize
        val stringSize = nextBlock.blockSize - dataSize - 8
        val stringData = stream.readBytes(stringSize.toInt())
        stream.position = curPos
        val dirEntries = ArrayList<DirectoryEntry>()
        for(i in 0 until numDirectories) {
            val nameOffset = stream.readInt()
            val blockNum = stream.readInt()
            val name = getString(stringData, nameOffset)
            val dir = DirectoryEntry(this, name, blockNum, index)
            dirEntries.add(dir)
            children.add(dir)
        }

        for(i in 0 until numFiles) {
            val nameOffset = stream.readInt()
            val name = getString(stringData, nameOffset)
            val currentPosition = stream.position
            children.add(FileEntry(this, name, stream))
            stream.position = currentPosition + 52
        }

        for(dir in dirEntries) {
            dir.parseChildren()
        }
    }

    private fun getString(data: ByteArray, start: Int): String {
        val buffer = ArrayList<Byte>()
        var pos = start
        var b = data[pos++]
        while(b != 0.toByte()) {
            buffer.add(b)
            b = data[pos++]
        }

        return String(buffer.toByteArray(), StandardCharsets.UTF_8)
    }

    override fun toString(): String {
        return "DirectoryEntry<name=$name, children=${children.size}>"
    }
}

class FileEntry(parent: FileSystemEntry?, name: String, stream: LittleEndianStream) : FileSystemEntry(parent, name) {
    val flags: Int
    val compressedSize: Long
    val uncompressedSize: Long
    val hash: ByteArray

    init {
        flags = stream.readInt()
        stream.position += 8
        uncompressedSize = stream.readLong()
        compressedSize = stream.readLong()
        hash = stream.readBytes(20)
    }

    override fun toString(): String {
        return "FileEntry<name=$name>"
    }
}

@Component
@Scope("prototype")
class IndexParser {
    companion object {
        fun parseHeader(input: LittleEndianStream): ArchiveIndexInfo {
            val parser = IndexParser()
            parser.load(input)
            return parser.indexInfo
        }

        val FILE_SIGNATURE = 0x5041434b
        val AIDX_SIGNATURE = 0x41494458
    }

    internal lateinit var stream: LittleEndianStream
    private lateinit var indexInfo: ArchiveIndexInfo
    lateinit var fileRoot: DirectoryEntry
    private val directoryList = ArrayList<PackDirectoryHeader>()

    fun getDirectoryHeader(index: Int): PackDirectoryHeader {
        return directoryList[index]
    }

    fun load(stream: LittleEndianStream) {
        this.stream = stream

        val signature = stream.readInt()
        if (signature != FILE_SIGNATURE) {
            throw IllegalArgumentException("Cannot parse index file. Signature was ${signature.toString(16)}, expected ${FILE_SIGNATURE.toString(16)}")
        }

        val version = stream.readInt()
        if (version != 1) {
            throw IllegalArgumentException("Only version 1 archives are supported, got $version")
        }

        stream.position += 528
        val offsetDirectoryTable = stream.readLong()
        val dirTableCount = stream.readInt()

        stream.position = offsetDirectoryTable.toInt()
        var aidxFound = false
        for (i in 0 until dirTableCount) {
            val dirEntry = PackDirectoryHeader(stream.readLong(), stream.readLong())
            directoryList.add(dirEntry)
            if (dirEntry.blockSize != 16L) {
                continue
            }

            val posOld = stream.position
            stream.position = dirEntry.offset.toInt()
            val magic = stream.readInt()
            if (magic == AIDX_SIGNATURE) {
                aidxFound = true
                indexInfo = ArchiveIndexInfo(magic, stream.readInt(), stream.readInt(), stream.readInt())
            }
            stream.position = posOld
        }

        if (!aidxFound) {
            throw IllegalArgumentException("Unable to find AIDX entry")
        }
    }

    fun loadIndexTree() {
        fileRoot = DirectoryEntry(null, "", indexInfo.rootBlock, this)
        fileRoot.parseChildren()
    }

    fun getFile(path: String) = fileRoot.getFile(path) as? FileEntry
}

abstract class FileHandler {
    abstract fun initialize(file: File?): CompletableFuture<Void>
    abstract fun getFileRoot(): DirectoryEntry
    abstract fun loadFile(entry: FileEntry): CompletableFuture<LittleEndianStream>
}