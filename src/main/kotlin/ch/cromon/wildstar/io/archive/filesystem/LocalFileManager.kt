package ch.cromon.wildstar.io.archive.filesystem

import ch.cromon.wildstar.io.LittleEndianStream
import ch.cromon.wildstar.io.archive.DirectoryEntry
import ch.cromon.wildstar.io.archive.FileEntry
import ch.cromon.wildstar.io.archive.FileHandler
import ch.cromon.wildstar.io.archive.IndexParser
import ch.cromon.wildstar.io.archive.cdn.ArchiveLoadEvent
import ch.cromon.wildstar.utils.changeExtension
import org.apache.commons.io.IOUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Component
import java.io.File
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.RandomAccessFile
import java.util.concurrent.CompletableFuture

@Component
@Qualifier("LOCAL")
class LocalFileManager: FileHandler() {
    @Autowired
    private lateinit var indexParser: IndexParser

    @Autowired
    private lateinit var publisher: ApplicationEventPublisher

    @Autowired
    private lateinit var archiveParser: ArchiveParser

    override fun initialize(file: File?): CompletableFuture<Void> {
        return CompletableFuture.runAsync {
            file ?: throw IllegalArgumentException("File must be selected")

            val archiveFile = file.changeExtension("archive")

            updateStatus("Parsing ClientData.index")
            FileInputStream(file).use {
                val inputStream = LittleEndianStream(IOUtils.toByteArray(it))
                indexParser.load(inputStream)
                indexParser.loadIndexTree()
            }

            updateStatus("Parsing ClientData.archive")
            archiveParser.parse(RandomAccessFile(archiveFile, "r"))
        }
    }

    override fun getFileRoot(): DirectoryEntry {
        return indexParser.fileRoot
    }

    override fun loadFile(entry: FileEntry): CompletableFuture<LittleEndianStream> {
        return CompletableFuture.supplyAsync {
            archiveParser.findFile(entry) ?: throw FileNotFoundException("File ${entry.getFullPath()} not found")
        }
    }

    private fun updateStatus(status: String) {
        publisher.publishEvent(ArchiveLoadEvent(this, status))
    }
}