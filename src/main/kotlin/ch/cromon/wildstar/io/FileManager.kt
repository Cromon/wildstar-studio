package ch.cromon.wildstar.io

import ch.cromon.wildstar.io.archive.FileEntry
import ch.cromon.wildstar.io.archive.FileHandler
import ch.cromon.wildstar.io.archive.FileSystemEntry
import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.BeanFactoryAnnotationUtils
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationEvent
import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component
import java.io.File
import java.util.concurrent.CompletableFuture

class FileLoadCompleteEvent(source: Any) : ApplicationEvent(source)

@Component
@Scope("singleton")
class FileManager {
    @Autowired
    private lateinit var logger: Logger

    @Autowired
    private lateinit var context: ApplicationContext

    @Autowired
    private lateinit var publisher: ApplicationEventPublisher

    private lateinit var fileHandler: FileHandler

    fun initFromCdn(): CompletableFuture<Void> {
        fileHandler = BeanFactoryAnnotationUtils.qualifiedBeanOfType(context.autowireCapableBeanFactory, FileHandler::class.java, "CDN")
        return fileHandler.initialize(null).thenRun({
            publisher.publishEvent(FileLoadCompleteEvent(this))
        })
    }

    fun initFromLocal(file: File): CompletableFuture<Void> {
        fileHandler = BeanFactoryAnnotationUtils.qualifiedBeanOfType(context.autowireCapableBeanFactory, FileHandler::class.java, "LOCAL")
        return fileHandler.initialize(file).thenRun {
            publisher.publishEvent(FileLoadCompleteEvent(this))
        }
    }

    fun getRootChildren(): Collection<FileSystemEntry> {
        return fileHandler.getFileRoot().children
    }

    fun openFile(entry: FileEntry): CompletableFuture<LittleEndianStream> {
        return fileHandler.loadFile(entry)
    }
}