package ch.cromon.wildstar.gx

import org.lwjgl.opengl.GL13.GL_TEXTURE0
import org.lwjgl.opengl.GL13.glActiveTexture

class TextureInput {
    lateinit var program: Program
    private val textureMap = HashMap<Int, Texture>()

    fun add(index: Int, texture: Texture) {
        textureMap[index] = texture
    }

    fun bind() {
        var index = 0
        textureMap.forEach { uniform, texture ->
            glActiveTexture(GL_TEXTURE0 + index)
            texture.bind()
            program.setInt(uniform, index)
            ++index
        }
    }
}