package ch.cromon.wildstar.gx

import org.lwjgl.opengl.GL11.*


enum class Topology(val native: Int) {
    TRIANGLES(GL_TRIANGLES),
    TRIANGLE_STRIP(GL_TRIANGLE_STRIP),
    TRIANGLE_FAN(GL_TRIANGLE_FAN),
    LINE_STRIP(GL_LINE_STRIP),
    QUADS(GL_QUADS)
}