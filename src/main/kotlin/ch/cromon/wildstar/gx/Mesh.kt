package ch.cromon.wildstar.gx

import org.lwjgl.opengl.GL11.glDrawElements


class Mesh {
    private val elements = ArrayList<VertexElement>()
    var vertexBuffer = VertexBuffer()
    var indexBuffer = IndexBuffer(IndexType.UINT16)

    val textureInput = TextureInput()

    var blendMode = BlendMode.NONE

    lateinit var program: Program
    var topology = Topology.TRIANGLES
    var indexCount = 0

    fun addElement(element: VertexElement) {
        elements.add(element)
    }

    fun finalize() {
        var totalSize = 0
        elements.forEach {
            it.bindToProgram(program, totalSize)
            totalSize += it.byteWidth
        }

        elements.forEach {
            it.stride = totalSize
        }

        textureInput.program = program
    }

    fun render() {
        program.use()

        vertexBuffer.bind()
        indexBuffer.bind()

        elements.forEach {
            it.apply()
        }

        textureInput.bind()
        blendMode.apply()
        glDrawElements(topology.native, indexCount, indexBuffer.indexType.native, 0)
    }
}