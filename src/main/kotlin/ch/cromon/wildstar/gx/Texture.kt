package ch.cromon.wildstar.gx

import ch.cromon.wildstar.io.ImageSource
import org.lwjgl.BufferUtils
import org.lwjgl.opengl.GL11.*
import org.lwjgl.opengl.GL12.GL_CLAMP_TO_EDGE
import org.lwjgl.opengl.GL13.glCompressedTexImage2D
import java.nio.ByteBuffer

enum class TextureFilter(val native: Int) {
    NEAREST(GL_NEAREST),
    LINEAR(GL_LINEAR)
}

class Texture {
    private var texture = defaultTexture

    fun loadLayer(layer: Int, width: Int, height: Int, format: Int, compressed: Boolean, data: ByteArray) {
        if(texture == defaultTexture) {
            loadTexture(true)
        }

        val buffer = BufferUtils.createByteBuffer(data.size)
        buffer.put(data).flip()

        bind()
        if(compressed) {
            glCompressedTexImage2D(GL_TEXTURE_2D, layer, format, width, height, 0, buffer)
        } else {
            glTexImage2D(GL_TEXTURE_2D, layer, format, width, height, 0, format, GL_UNSIGNED_BYTE, buffer)
        }
    }

    fun setFilter(minFilter: TextureFilter, maxFilter: TextureFilter) {
        if(texture == defaultTexture) {
            loadTexture(false)
        }

        bind()
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minFilter.native)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, maxFilter.native)
    }

    fun loadArgb(source: ImageSource) {
        val data = source.buffer
        if(data.isEmpty()) {
            return
        }

        if(texture == defaultTexture) {
            loadTexture(false)
        }

        bind()
        val buffer = BufferUtils.createByteBuffer(data.size)
        buffer.put(data)
        buffer.flip()
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, source.width, source.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer)
    }

    fun loadArgb(w: Int, h: Int, data: ByteBuffer) {
        if(texture == defaultTexture) {
            loadTexture(false)
        }

        bind()
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, data)
    }

    fun bind() {
        glBindTexture(GL_TEXTURE_2D, texture)
    }

    private fun loadTexture(mipMap: Boolean) {
        val textures = intArrayOf(0)
        glGenTextures(textures)
        texture = textures[0]
        bind()
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, if(mipMap) GL_LINEAR_MIPMAP_LINEAR else GL_NEAREST)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, if(mipMap) GL_LINEAR else GL_NEAREST)
    }

    companion object {
        private var defaultTexture = 0

        fun initDefaultTexture() {
            val textures = intArrayOf(0)
            glGenTextures(textures)
            defaultTexture = textures[0]
            glBindTexture(GL_TEXTURE_2D, defaultTexture)
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE)
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)

            val colors = intArrayOf(
                    0xFFFF0000.toInt(), 0xFF00FF00.toInt(),
                    0xFF0000FF.toInt(), 0xFFFF7F3F.toInt()
            )
            val buffer = BufferUtils.createIntBuffer(colors.size)
            buffer.put(colors)
            buffer.flip()
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 2, 2, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer)
        }
    }
}