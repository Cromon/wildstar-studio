package ch.cromon.wildstar.gx

import org.lwjgl.BufferUtils
import org.lwjgl.opengl.GL11.*
import org.lwjgl.opengl.GL15.*
import java.nio.ByteBuffer
import java.nio.FloatBuffer
import java.nio.IntBuffer
import java.nio.ShortBuffer


abstract class Buffer(private val target: Int) {
    private val native: IntBuffer = BufferUtils.createIntBuffer(1)

    init {
        glGenBuffers(native)
    }

    fun delete() {
        glDeleteBuffers(native)
    }

    fun bind() {
        glBindBuffer(target, native.get(0))
    }

    fun data(data: FloatBuffer) {
        bind()
        glBufferData(target, data, GL_STATIC_DRAW)
    }

    fun data(data: ByteBuffer) {
        bind()
        glBufferData(target, data, GL_STATIC_DRAW)
    }

    fun data(data: ShortBuffer) {
        bind()
        glBufferData(target, data, GL_STATIC_DRAW)
    }

    fun data(data: IntBuffer) {
        bind()
        glBufferData(target, data, GL_STATIC_DRAW)
    }
}

enum class IndexType(val native: Int) {
    UINT8(GL_UNSIGNED_BYTE),
    UINT16(GL_UNSIGNED_SHORT),
    UINT32(GL_UNSIGNED_INT)
}

class VertexBuffer : Buffer(GL_ARRAY_BUFFER)
class IndexBuffer(val indexType: IndexType) : Buffer(GL_ELEMENT_ARRAY_BUFFER)

