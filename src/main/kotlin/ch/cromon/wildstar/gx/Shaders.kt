package ch.cromon.wildstar.gx

class Shader private constructor(private val fs: String, private val vs: String) {
    val fragmentShader: String
        get() = fs

    val vertexShader: String
        get() = vs

    companion object {
        val TEXT = Shader("shaders/TextFragment.glsl", "shaders/TextVertex.glsl")
        val QUAD = Shader("shaders/QuadFragment.glsl", "shaders/QuadVertex.glsl")
        val LINE = Shader("shaders/LineFragment.glsl", "shaders/LineVertex.glsl")
        val MEMORY_CONTROL = Shader("shaders/MemoryControlFragment.glsl", "shaders/MemoryControlVertex.glsl")
        val IMAGE = Shader("shaders/ImageFragment.glsl", "shaders/ImageVertex.glsl")
        val SYNTAX_TEXT = Shader("shaders/SyntaxTextFragment.glsl", "shaders/SyntaxTextVertex.glsl")
    }
}