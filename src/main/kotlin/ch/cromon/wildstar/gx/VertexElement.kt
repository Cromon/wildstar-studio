package ch.cromon.wildstar.gx

import org.lwjgl.opengl.GL20.glEnableVertexAttribArray
import org.lwjgl.opengl.GL20.glVertexAttribPointer


class VertexElement(
        private val semantic: String,
        private val index: Int,
        private val components: Int,
        private val dataType: DataType,
        private val normalized: Boolean) {
    private var attributeIndex: Int = -1

    var stride: Int = 0
    private var offset: Int = 0

    val byteWidth: Int
        get() = when(dataType) {
            DataType.BYTE -> components
            DataType.FLOAT -> components * 4
        }

    constructor(semantic: String, index: Int, components: Int) : this(semantic, index, components, DataType.FLOAT, false)

    fun bindToProgram(program: Program, offset: Int) {
        this.offset = offset
        attributeIndex = program.getAttribute("$semantic$index")
        if(attributeIndex < 0) {
            throw IllegalArgumentException("Attribute $semantic$index not found in program")
        }
    }

    fun apply() {
        glEnableVertexAttribArray(attributeIndex)
        glVertexAttribPointer(attributeIndex, components, dataType.native, normalized, stride, offset.toLong())
    }
}