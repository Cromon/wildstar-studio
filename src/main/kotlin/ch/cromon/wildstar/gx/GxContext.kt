package ch.cromon.wildstar.gx

import org.lwjgl.opengl.GL11.*
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component


@Component
@Scope("singleton")
class GxContext {
    companion object {
        private val LOG = LoggerFactory.getLogger(GxContext::class.java)
    }

    fun beginFrame() {
        glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT)
    }

    fun onInitialize() {
        LOG.debug("Initializing GL context")

        glClearColor(0.0f, 0.0f, 0.0f, 1.0f)
        glEnable(GL_CULL_FACE)
        glFrontFace(GL_CCW)

        Texture.initDefaultTexture()
    }

    fun onResize(width: Int, height: Int) {
        glViewport(0, 0, width, height)
    }
}