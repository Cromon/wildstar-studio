package ch.cromon.wildstar.gx

import glm.mat4x4.Mat4
import glm.vec2.Vec2
import glm.vec3.Vec3
import glm.vec4.Vec4
import org.apache.commons.io.IOUtils
import org.lwjgl.BufferUtils
import org.lwjgl.opengl.GL11.GL_TRUE
import org.lwjgl.opengl.GL20.*
import org.slf4j.LoggerFactory


class Program {
    companion object {
        private val LOG = LoggerFactory.getLogger(Program::class.java)
        private var currentProgram = -1
        private var matrixBuffer = BufferUtils.createFloatBuffer(16)
    }

    private val vertexShader: Int
    private val fragmentShader: Int
    private val program: Int

    init {
        vertexShader = glCreateShader(GL_VERTEX_SHADER)
        fragmentShader = glCreateShader(GL_FRAGMENT_SHADER)
        program = glCreateProgram()

        glAttachShader(program, vertexShader)
        glAttachShader(program, fragmentShader)
    }

    fun use() {
        if(currentProgram == program) {
            return
        }

        currentProgram = program
        glUseProgram(program)
    }

    fun getUniform(name: String) : Int = glGetUniformLocation(program, name)

    fun getAttribute(name: String) : Int = glGetAttribLocation(program, name)

    fun setInt(index: Int, value: Int) {
        forUniform(index)
        glUniform1i(index, value)
    }

    fun setFloat(index: Int, value: Float) {
        forUniform(index)
        glUniform1f(index, value)
    }

    fun setVec2(index: Int, value: Vec2) {
        forUniform(index)
        glUniform2f(index, value.x, value.y)
    }

    fun setVec3(index: Int, value: Vec3) {
        forUniform(index)
        glUniform3f(index, value.x, value.y, value.z)
    }

    fun setVec4(index: Int, value: Vec4) {
        forUniform(index)
        glUniform4f(index, value.x, value.y, value.z, value.w)
    }

    fun setMatrix(index: Int, value: Mat4) {
        forUniform(index)
        glUniformMatrix4fv(index, false, value.to(matrixBuffer))
    }

    fun link() {
        val arrayData = intArrayOf(0)
        glLinkProgram(program)
        glGetProgramiv(program, GL_LINK_STATUS, arrayData)
        if(arrayData[0] != GL_TRUE) {
            glGetProgramiv(program, GL_INFO_LOG_LENGTH, arrayData)
            val logLength = arrayData[0]
            var linkError = "Program linkage failed with unknown error"
            if(logLength > 0) {
                val strBuffer = BufferUtils.createByteBuffer(logLength)
                glGetProgramInfoLog(program, arrayData, strBuffer)
                if(arrayData[0] > 0) {
                    linkError = String(strBuffer.array(), Charsets.UTF_8)
                }
            }

            LOG.error("Error linking program: {}", linkError)
            throw IllegalStateException("Error linking program: $linkError")
        }
    }

    private fun compileVertexShader(source: String) {
        loadShader(vertexShader, source)
    }

    private fun compileFragmentShader(source: String) {
        loadShader(fragmentShader, source)
    }

    fun compileFromResource(shader: Shader) {
        compileVertexShaderFromResource(shader.vertexShader)
        compileFragmentShaderFromResource(shader.fragmentShader)
    }

    fun compileVertexShaderFromResource(resource: String) {
        Program::class.java.classLoader.getResourceAsStream(resource).use {
            compileVertexShader(IOUtils.toString(it, Charsets.UTF_8))
        }
    }

    fun compileFragmentShaderFromResource(resource: String) {
        Program::class.java.classLoader.getResourceAsStream(resource).use {
            compileFragmentShader(IOUtils.toString(it, Charsets.UTF_8))
        }
    }

    private fun loadShader(shader: Int, source: String) {
        val arrayData = intArrayOf(0)
        glShaderSource(shader, source)
        glCompileShader(shader)
        glGetShaderiv(shader, GL_COMPILE_STATUS, arrayData)
        if(arrayData[0] != GL_TRUE) {
            glGetShaderiv(shader, GL_INFO_LOG_LENGTH, arrayData)
            val logLength = arrayData[0]
            var shaderError = "Shader compilation failed with unknown error"
            if(logLength > 0) {
                val strBuffer = BufferUtils.createByteBuffer(logLength)
                glGetShaderInfoLog(shader, arrayData, strBuffer)
                if(arrayData[0] > 0) {
                    val arr = ByteArray(logLength)
                    strBuffer.get(arr, 0, logLength)
                    shaderError = String(arr, Charsets.UTF_8)
                }
            }

            LOG.error("Error compiling shader: {}", shaderError)
            throw IllegalArgumentException(shaderError)
        }
    }

    private fun forUniform(index: Int) {
        checkIndex(index)
        use()
    }

    private fun checkIndex(index: Int) {
        if(index < 0) {
            throw IllegalArgumentException("Cannot set unknown uniform")
        }
    }
}