package ch.cromon.wildstar.utils

import glm.mat4x4.Mat4
import glm.vec2.Vec2
import glm.vec3.Vec3
import glm.vec4.Vec4
import java.awt.Color

fun Mat4.mulVec2(vector: Vec2): Vec2 {
    val tmp = this * Vec4(vector, 0.0, 1.0)
    return tmp.run {
        Vec2(x / w, y / w)
    }
}

fun Mat4.mulVec3(vector: Vec3): Vec3 {
    val tmp = this * Vec4(vector, 1.0f)
    return tmp.run {
        Vec3(x / w, y / w, z / w)
    }
}

fun Vec3.toXrgb(): Int {
    val r = (this.x * 255).toInt()
    val g = (this.y * 255).toInt()
    val b = (this.z * 255).toInt()

    return 0xFF000000.toInt() or ((r and 0xFF) shl 16) or ((g and 0xFF) shl 8) or (b and 0xFF)

}

fun Float.format(digits: Int) = java.lang.String.format("%.${digits}f", this)!!

fun Color.toVec4(): Vec4 = Vec4(this.red / 255.0f, this.green / 255.0f, this.blue / 255.0f, this.alpha / 255.0f)