package ch.cromon.wildstar.utils

import glm.mat4x4.Mat4
import glm.vec3.Vec3

class BoundingBox(minValue: Vec3 = Vec3(), maxValue: Vec3 = Vec3()) {
    var min = minValue
    var max = maxValue

    fun intersects(plane: Plane): Boolean {
        val x = if(plane.normal.x > 0) max.x else min.x
        val y = if(plane.normal.y > 0) max.y else min.y
        val z = if(plane.normal.z > 0) max.z else min.z
        val dp = plane.normal.x * x + plane.normal.y * y + plane.normal.z * z
        return dp >= -plane.distance
    }
}

class Plane(var normal: Vec3 = Vec3(), var distance: Float = 0.0f) {
    constructor(a: Float, b: Float, c: Float, d: Float) : this(Vec3(a, b, c), d)
}

class Frustum {
    private val planes = Array(6, { Plane() })

    private var view = Mat4()
    private var projection = Mat4()

    fun intersect(boundingBox: BoundingBox): Boolean = planes.all { boundingBox.intersects(it) }

    fun updateView(matrix: Mat4) {
        view = matrix
        onMatrixUpdated()
    }

    fun updateProjection(matrix: Mat4) {
        projection = matrix
        onMatrixUpdated()
    }

    private fun onMatrixUpdated() {
        val m = (projection * view).transpose_()
        // left
        planes[0] = Plane(
                m[0][0] + m[3][0],
                m[0][1] + m[3][1],
                m[0][2] + m[3][2],
                m[0][3] + m[3][3]
        )

        // right
        planes[1] = Plane(
                -m[0][0] + m[3][0],
                -m[0][1] + m[3][1],
                -m[0][2] + m[3][2],
                -m[0][3] + m[3][3]
        )

        // bottom
        planes[2] = Plane(
                m[1][0] + m[3][0],
                m[1][1] + m[3][1],
                m[1][2] + m[3][2],
                m[1][3] + m[3][3]
        )

        // top
        planes[2] = Plane(
                -m[1][0] + m[3][0],
                -m[1][1] + m[3][1],
                -m[1][2] + m[3][2],
                -m[1][3] + m[3][3]
        )

        // near
        planes[2] = Plane(
                m[2][0] + m[3][0],
                m[2][1] + m[3][1],
                m[2][2] + m[3][2],
                m[2][3] + m[3][3]
        )

        // far
        planes[2] = Plane(
                -m[2][0] + m[3][0],
                -m[2][1] + m[3][1],
                -m[2][2] + m[3][2],
                -m[2][3] + m[3][3]
        )
    }
}