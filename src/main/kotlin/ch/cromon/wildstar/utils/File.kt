package ch.cromon.wildstar.utils

import org.apache.commons.lang3.StringUtils
import java.io.File
import java.io.RandomAccessFile
import kotlin.concurrent.getOrSet

val shortBuffer = ThreadLocal<ByteArray>()
val intBuffer = ThreadLocal<ByteArray>()
val longBuffer = ThreadLocal<ByteArray>()

fun File.changeExtension(newExtension: String): File {
    val extension = this.extension
    if(StringUtils.isBlank(extension)) {
        return File(this.absolutePath + ".$newExtension")
    }

    return File(this.absolutePath.substring(0, this.absolutePath.length - extension.length) + newExtension)
}

fun RandomAccessFile.readShortLE(): Short {
    val buffer = shortBuffer.getOrSet { ByteArray(2) }
    readFully(buffer)
    val b0 = buffer[0].toInt()
    val b1 = buffer[1].toInt()

    return ((b0 and 0xFF) or ((b1 and 0xFF) shl 8)).toShort()
}

fun RandomAccessFile.readIntLE(): Int {
    val buffer = intBuffer.getOrSet { ByteArray(4) }
    readFully(buffer)
    val b0 = buffer[0].toInt()
    val b1 = buffer[1].toInt()
    val b2 = buffer[2].toInt()
    val b3 = buffer[3].toInt()

    return (b0 and 0xFF) or ((b1 and 0xFF) shl 8) or ((b2 and 0xFF) shl 16) or ((b3 and 0xFF) shl 24)
}

fun RandomAccessFile.readLongLE(): Long {
    val buffer = longBuffer.getOrSet { ByteArray(8) }
    readFully(buffer)
    val b0 = buffer[0].toLong()
    val b1 = buffer[1].toLong()
    val b2 = buffer[2].toLong()
    val b3 = buffer[3].toLong()
    val b4 = buffer[4].toLong()
    val b5 = buffer[5].toLong()
    val b6 = buffer[6].toLong()
    val b7 = buffer[7].toLong()

    return (b0 and 0xFF) or ((b1 and 0xFF) shl 8) or ((b2 and 0xFF) shl 16) or ((b3 and 0xFF) shl 24) or
            ((b4 and 0xFF) shl 32) or ((b5 and 0xFF) shl 40) or ((b6 and 0xFF) shl 48) or ((b7 and 0xFF) shl 56)
}

