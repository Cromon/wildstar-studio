package ch.cromon.wildstar.utils

import glm.vec2.Vec2


fun pointInQuad(point: Vec2, topLeft: Vec2, bottomRight: Vec2): Boolean {
    return point.x >= topLeft.x && point.x <= bottomRight.x &&
            point.y >= topLeft.y && point.y <= bottomRight.y
}