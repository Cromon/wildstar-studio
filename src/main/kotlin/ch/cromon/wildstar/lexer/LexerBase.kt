package ch.cromon.wildstar.lexer

import glm.vec2.Vec2
import org.apache.commons.lang3.StringUtils
import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component
import java.awt.Color
import java.awt.Font
import java.awt.Graphics2D
import java.awt.RenderingHints
import java.awt.font.LineBreakMeasurer
import java.awt.font.TextAttribute
import java.awt.image.BufferedImage
import java.text.AttributedString
import java.util.regex.Pattern

val defaultFont = Font("Lucida Console", Font.PLAIN, 22)

abstract class BaseToken(val content: String, val startIndex: Int, val endIndex: Int) {
    open fun getFont(): Font = defaultFont
    open fun isWhitespace(): Boolean {
        return false
    }

    open fun isNewLine(): Boolean {
        return false
    }

    open fun getColor(): Color {
        return Color.WHITE
    }
}

data class RenderedLine(val image: BufferedImage, val bounds: Vec2, val content: String)

@Scope("prototype")
abstract class BaseLexer {
    internal val tokens = ArrayList<BaseToken>()

    protected var lineLayouts: List<AttributedString> = ArrayList()
    private var lineContents = ArrayList<String>()

    private val workingImage = BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB)
    private val workingGraphics = workingImage.createGraphics()

    @Autowired
    private lateinit var logger: Logger

    internal abstract fun lex(input: String)
    internal abstract fun getFullText(): String

    open fun getLineCount() = getLines().size

    open fun getLines(): List<String> {
        if(lineContents.isNotEmpty()) {
            return lineContents
        }

        lineContents.addAll(getFullText().split(Pattern.compile("$", Pattern.MULTILINE)))
        return lineContents
    }

    protected open fun mapTokensToLines(lines: List<String>) {
        var curLineStart = 0
        var curLineEnd = lines[0].length
        var lineIndex = 0
        tokens.forEach {
            val start = it.startIndex
            while (start >= curLineEnd) {
                if (lineIndex + 1 >= lines.size) {
                    throw IllegalStateException("Tokens are exceeding number of lines - something is wrong...")
                }
                curLineStart = curLineEnd
                curLineEnd = lines[++lineIndex].length + curLineStart
            }

            while(it.endIndex > curLineEnd) {
                if (lineIndex + 1 >= lines.size) {
                    throw IllegalStateException("Tokens are exceeding number of lines - something is wrong...")
                }
                val startIndex = Math.max(it.startIndex, curLineStart) - curLineStart
                val endIndex = curLineEnd - curLineStart
                lineLayouts[lineIndex].addAttribute(TextAttribute.FONT, it.getFont(), startIndex, endIndex)
                lineLayouts[lineIndex].addAttribute(TextAttribute.FOREGROUND, it.getColor(), startIndex, endIndex)
                curLineStart = curLineEnd
                curLineEnd = lines[++lineIndex].length + curLineStart
            }

            val startIndex = Math.max(it.startIndex, curLineStart) - curLineStart
            lineLayouts[lineIndex].addAttribute(TextAttribute.FONT, it.getFont(), startIndex, it.endIndex - curLineStart)
            lineLayouts[lineIndex].addAttribute(TextAttribute.FOREGROUND, it.getColor(), startIndex, it.endIndex - curLineStart)
        }
    }

    private fun layout(): List<AttributedString> {
        if (lineLayouts.isNotEmpty()) {
            return lineLayouts
        }

        val lines = getLines()
        lineLayouts = lines.map({ AttributedString(it) }).toList()
        if (lines.isEmpty()) {
            return lineLayouts
        }

        mapTokensToLines(lines)

        return lineLayouts
    }

    fun getHeight(graphics: Graphics2D, width: Int) = layout().sumByDouble { getLineHeight(it, graphics, width).toDouble() }.toInt()

    fun draw(graphics: Graphics2D, width: Int) {
        var y = 0.0f
        layout().forEach { y += drawLine(it, graphics, width, y) }
    }

    fun drawLine(lineIndex: Int, width: Int): RenderedLine {
        val lines = layout()
        if(lineIndex >= lines.size) {
            logger.warn("Attempted to render line that is out of bounds: {} (but there are only {} lines", lineIndex, lines.size)
            throw IllegalArgumentException("line index out of bounds")
        }

        val lineTexts = getLines()

        val line = lines[lineIndex]
        val height = getLineHeight(line, workingGraphics, width)
        if(height.toInt() <= 0) {
            return RenderedLine(BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB), Vec2(width, 0), lineTexts[lineIndex])
        }

        val image = BufferedImage(width, height.toInt(), BufferedImage.TYPE_INT_ARGB)
        val graphics = image.createGraphics()
        graphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON)

        val sizeY = drawLine(line, graphics, width)

        return RenderedLine(image, Vec2(width, sizeY), lineTexts[lineIndex])
    }

    private fun getLineHeight(line: AttributedString, graphics: Graphics2D, width: Int): Float {
        val iterator = line.iterator
        if(iterator.beginIndex >= iterator.endIndex) {
            return 0.0f
        }

        val end = iterator.endIndex
        val measurer = LineBreakMeasurer(iterator, graphics.fontRenderContext)
        measurer.position = iterator.beginIndex

        var sizeY = 0.0f
        while(measurer.position < end) {
            val layout = measurer.nextLayout(width.toFloat())
            sizeY += layout.ascent + layout.descent + layout.leading
        }

        return sizeY
    }

    private fun drawLine(line: AttributedString, graphics: Graphics2D, width: Int, offset: Float = 0.0f): Float {
        val iterator = line.iterator
        if(iterator.beginIndex >= iterator.endIndex) {
            return 0.0f
        }

        val end = iterator.endIndex
        val measurer = LineBreakMeasurer(iterator, graphics.fontRenderContext)
        measurer.position = iterator.beginIndex

        var sizeY = 0.0f
        while(measurer.position < end) {
            val layout = measurer.nextLayout(width.toFloat())
            val dx = if (layout.isLeftToRight) 0.0f else width - layout.advance
            sizeY += layout.ascent
            layout.draw(graphics, dx, sizeY + offset)
            sizeY += layout.descent + layout.leading
        }

        return sizeY
    }
}

@Component
@Qualifier("lexer.text")
@Scope("prototype")
class TextLexer : BaseLexer() {
    private lateinit var content: String
    private val font = Font("Lucida Console", Font.PLAIN, 22)

    override fun lex(input: String) {
        content = input
    }

    override fun getFullText() = content

    override fun mapTokensToLines(lines: List<String>) {
        lineLayouts.forEachIndexed {
            index, layout ->
            if(StringUtils.isEmpty(lines[index])) {
                return@forEachIndexed
            }

            layout.addAttribute(TextAttribute.FONT, font)
        }
    }
}