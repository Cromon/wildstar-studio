package lexer;

import ch.cromon.wildstar.lexer.xml.*;
import java.util.*;

%%
%class XmlLex
%type Void
%public
%final
%unicode

%{
    List<Token> tokenList = new ArrayList<>();

    private void makeToken(TokenType type) {
        tokenList.add(new Token(type, yytext().toString(), zzStartRead, zzStartRead + yylength()));
    }

    public List<Token> getTokens() { return Collections.unmodifiableList(tokenList); }
%}

LINE_END = \r|\n|\r\n
COMMENT = "<!--" [^-]* ~"-->"
WHITESPACE = {LINE_END} | [ \t\f]+
DIGIT = [0-9]
LETTER = [a-zA-Z_]
ATTRIBUTE_NAME = {LETTER}({DIGIT}|{LETTER})+
ATTRIBUTE_VALUE = [^\"]+
TAG_NAME = {LETTER}({DIGIT}|{LETTER})+
TAG_VALUE = [^<&]+


%state DOC_TYPE
%state TAG
%state END_TAG
%state ATTRIB
%state ATTRIB_VALUE
%state TAG_INLINE
%state DOCTYPE_ATTR
%state DOCTYPE_ATTR_VALUE

%%

<YYINITIAL> {
    {COMMENT} { makeToken(TokenType.COMMENT); }
    "<?xml" { yybegin(DOC_TYPE); makeToken(TokenType.DOCTYPE_LIMIT); }
    "</" { yybegin(END_TAG); makeToken(TokenType.TAG_NAME); }
    "<" { yybegin(TAG); makeToken(TokenType.TAG_NAME); }
    {TAG_VALUE} { makeToken(TokenType.TEXT); }
    {WHITESPACE} { makeToken(TokenType.WHITESPACE); }
}

<END_TAG> {
    ">" { yybegin(YYINITIAL); makeToken(TokenType.TAG_NAME); }
    {TAG_NAME} { makeToken(TokenType.TAG_NAME); }
}

<TAG> {
    {WHITESPACE} { makeToken(TokenType.WHITESPACE); }
    {TAG_NAME} { yybegin(TAG_INLINE); makeToken(TokenType.TAG_NAME); }
    ">" { yybegin(YYINITIAL); makeToken(TokenType.TAG_NAME); }
}

<TAG_INLINE> {
    {ATTRIBUTE_NAME} { yybegin(ATTRIB); makeToken(TokenType.ATTRIBUTE_NAME); }
    "/>" { yybegin(YYINITIAL); makeToken(TokenType.TAG_NAME); }
    ">" { yybegin(YYINITIAL); makeToken(TokenType.TAG_NAME); }
    {LINE_END} { makeToken(TokenType.WHITESPACE); }
    {WHITESPACE} { makeToken(TokenType.WHITESPACE); }
}

<DOC_TYPE> {
    {WHITESPACE} { makeToken(TokenType.WHITESPACE); }
    {ATTRIBUTE_NAME} { yybegin(DOCTYPE_ATTR); makeToken(TokenType.ATTRIBUTE_NAME); }
    "?>" { yybegin(YYINITIAL); makeToken(TokenType.DOCTYPE_LIMIT); }
}

<DOCTYPE_ATTR> {
    {WHITESPACE}+={WHITESPACE}+\" { yybegin(DOCTYPE_ATTR_VALUE); makeToken(TokenType.ATTRIBUTE_VALUE); }
    =\" { yybegin(DOCTYPE_ATTR_VALUE); makeToken(TokenType.ATTRIBUTE_VALUE); }
}

<DOCTYPE_ATTR_VALUE> {
    {ATTRIBUTE_VALUE} {  makeToken(TokenType.ATTRIBUTE_VALUE);}
    \" { yybegin(DOC_TYPE);  makeToken(TokenType.ATTRIBUTE_VALUE); }
}

<ATTRIB> {
    {WHITESPACE}+={WHITESPACE}+\" { yybegin(ATTRIB_VALUE);  makeToken(TokenType.ATTRIBUTE_VALUE); }
    =\" { yybegin(ATTRIB_VALUE);  makeToken(TokenType.ATTRIBUTE_VALUE); }
}

<ATTRIB_VALUE> {
    {ATTRIBUTE_VALUE} {  makeToken(TokenType.ATTRIBUTE_VALUE); }
    \" { yybegin(TAG_INLINE);  makeToken(TokenType.ATTRIBUTE_VALUE); }
}

. { }

{LINE_END} { makeToken(TokenType.WHITESPACE); }