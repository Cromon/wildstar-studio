package ch.cromon.wildstar.lexer.xml

enum class TokenType {
    WHITESPACE,
    COMMENT,
    TEXT,
    TAG_NAME,
    ATTRIBUTE_NAME,
    ATTRIBUTE_VALUE,
    DOCTYPE_LIMIT
}
