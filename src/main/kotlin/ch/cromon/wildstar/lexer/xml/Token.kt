package ch.cromon.wildstar.lexer.xml

import ch.cromon.wildstar.lexer.BaseLexer
import ch.cromon.wildstar.lexer.BaseToken
import ch.cromon.wildstar.lexer.xml.TokenType.*
import lexers.XmlLex
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component
import java.awt.Color
import java.awt.Font

class CommentToken(content: String, start: Int, end: Int) : BaseToken(content, start, end) {
    override fun getFont(): Font {
        val baseFont = super.getFont()
        return baseFont.deriveFont(Font.ITALIC)
    }

    override fun getColor(): Color {
        return Color.getHSBColor(95.0f / 365.0f, 0.3778f, 0.7644f)
    }
}

class WhiteSpaceToken(content: String, start: Int, end: Int): BaseToken(content, start, end) {
    override fun isWhitespace() = true
}

class TagNameToken(content: String, start: Int, end: Int): BaseToken(content, start, end) {
    override fun getFont(): Font {
        val baseFont = super.getFont()
        return baseFont.deriveFont(Font.BOLD)
    }

    override fun getColor(): Color {
        return Color.getHSBColor(355.0f / 365.0f, 0.52f, 0.88f)
    }
}

class AttributeNameToken(content: String, start: Int, end: Int): BaseToken(content, start, end) {
    override fun getColor() = Color.getHSBColor(5.0f / 365.0f, 0.6312f, 0.7452f)!!
}

class AttributeValueToken(content: String, start: Int, end: Int): BaseToken(content, start, end) {
    override fun getColor() = Color.getHSBColor(207.0f / 365.0f, 0.5939f, 0.9388f)!!
}

class TextToken(content: String, start: Int, end: Int): BaseToken(content, start, end)

private val xmlTokenMap = hashMapOf(
        COMMENT to CommentToken::class.java,
        WHITESPACE to WhiteSpaceToken::class.java,
        TAG_NAME to TagNameToken::class.java,
        ATTRIBUTE_NAME to AttributeNameToken::class.java,
        ATTRIBUTE_VALUE to AttributeValueToken::class.java,
        DOCTYPE_LIMIT to TagNameToken::class.java,
        TEXT to TextToken::class.java
)

data class Token(val type: TokenType, val content: String, val start: Int, val end: Int)

@Component
@Scope("prototype")
@Qualifier("lexer.xml")
class XmlLexer : BaseLexer() {
    private val lexer = XmlLex(null)

    private lateinit var fullText: String

    override fun getFullText(): String {
        return fullText
    }

    override fun lex(input: String) {
        fullText = input
        tokens.clear()

        lexer.reset(input, 0, input.length, XmlLex.YYINITIAL)
        lexer.yylex()

        tokens.addAll(lexer.tokens.mapNotNull({
            xmlTokenMap[it.type]?.getConstructor(String::class.java, Int::class.java, Int::class.java)?.newInstance(it.content, it.start, it.end)
        }))
    }
}