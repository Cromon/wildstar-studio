package ch.cromon.wildstar.lexer

import lexers.XmlLex
import java.io.StringReader

class FileParser {
    private lateinit var lexer: XmlLex

    fun test(data: String) {
        val reader = StringReader(data)
        lexer = XmlLex(reader)
        lexer.reset(data, 0, data.length, XmlLex.YYINITIAL)
        lexer.yylex()
    }
}