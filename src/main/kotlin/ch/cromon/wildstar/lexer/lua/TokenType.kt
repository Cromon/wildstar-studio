package ch.cromon.wildstar.lexer.lua


enum class TokenType {
    KEYWORD,
    STRING,
    NUMBER_LITERAL,
    IDENTIFIER,
    COMMENT,
    WHITESPACE
}