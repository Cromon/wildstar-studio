package lexer;

import ch.cromon.wildstar.lexer.lua.*;
import static ch.cromon.wildstar.lexer.lua.TokenType.*;
import java.util.*;

%%
%class LuaLex
%type Void
%public
%final
%unicode

%{
    private int longLength = 0;
    private List<Token> tokenList = new ArrayList<>();

    private void makeToken(TokenType type) {
        tokenList.add(new Token(type, yytext().toString(), zzStartRead, zzStartRead + yylength()));
    }

    public List<Token> getTokens() { return Collections.unmodifiableList(tokenList); }
%}

%state STRING
%state SINGLE_QUOTE_STRING
%state LONG_COMMENT
%state LONG_STRING

NEW_LINE = \r|\r\n|\n
HEX_LITERAL = 0x[0-9a-fA-F]+
DIGIT = [0-9]
LETTER = [a-zA-Z_]
NUMBER_LITERAL = [0-9]+(\.{DIGIT}+)?([eE][+-]?{DIGIT}+)?
IDENTIFIER = {LETTER}({LETTER}|{DIGIT})*

WHITESPACE = {NEW_LINE}|[ \t\f]

COMMENT = --.*{NEW_LINE}

LONG_START = \[=*\[
LONG_END = ]=*]

%%

<YYINITIAL> {
     and      |
     break    |
     do       |
     else     |
     elseif   |
     end      |
     false    |
     for      |
     function |
     if       |
     in       |
     local    |
     nil      |
     not      |
     or       |
     repeat   |
     return   |
     then     |
     true     |
     until    |
     while      { makeToken(KEYWORD); }

     --\[\[ { yybegin(LONG_COMMENT); makeToken(COMMENT); }

     {HEX_LITERAL} { makeToken(NUMBER_LITERAL); }
     {NUMBER_LITERAL} { makeToken(NUMBER_LITERAL); }

     {IDENTIFIER} { makeToken(IDENTIFIER); }

     {COMMENT} { makeToken(COMMENT); }

     {LONG_START} { yybegin(LONG_STRING); makeToken(TokenType.STRING); longLength = yylength(); }

     \" { yybegin(STRING); makeToken(TokenType.STRING); }
     ' { yybegin(SINGLE_QUOTE_STRING); makeToken(TokenType.STRING); }
}

<LONG_STRING> {
    {LONG_END} {
        if(longLength == yylength()) {
            yybegin(YYINITIAL);
        }

        makeToken(TokenType.STRING);
    }
}

<STRING> {
    [^\"\\]+ { makeToken(TokenType.STRING); }
    \\\" { makeToken(TokenType.STRING); }
    \\ { makeToken(TokenType.STRING); }
    \" { yybegin(YYINITIAL); makeToken(TokenType.STRING); }
}

<SINGLE_QUOTE_STRING> {
    [^'\\] { makeToken(TokenType.STRING); }
    \\' { makeToken(TokenType.STRING); }
    \\ { makeToken(TokenType.STRING); }
    ' { yybegin(YYINITIAL); makeToken(TokenType.STRING); }
}

<LONG_COMMENT> {
    ]]-- { yybegin(YYINITIAL); makeToken(COMMENT); }
    .+ { makeToken(COMMENT); }
}

{WHITESPACE} { makeToken(WHITESPACE); }
[(){}\[\]+\-=><*/%\^#~;:,.a-zA-Z0-9]* { makeToken(IDENTIFIER); }
. { }