package ch.cromon.wildstar.lexer.lua

import ch.cromon.wildstar.lexer.BaseLexer
import ch.cromon.wildstar.lexer.BaseToken
import ch.cromon.wildstar.lexer.lua.TokenType.*
import lexers.LuaLex
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component
import java.awt.Color
import java.awt.Font


data class Token(val type: TokenType, val content: String, val start: Int, val end: Int)

class CommentToken(content: String, start: Int, end: Int) : BaseToken(content, start, end) {
    override fun getFont(): Font {
        val baseFont = super.getFont()
        return baseFont.deriveFont(Font.ITALIC)
    }

    override fun getColor(): Color {
        return Color.getHSBColor(95.0f / 365.0f, 0.3778f, 0.7644f)
    }
}

class WhiteSpaceToken(content: String, start: Int, end: Int) : BaseToken(content, start, end) {
    override fun isWhitespace() = true
}

class KeywordToken(content: String, start: Int, end: Int) : BaseToken(content, start, end) {
    override fun getFont(): Font {
        val baseFont = super.getFont()
        return baseFont.deriveFont(Font.BOLD)
    }

    override fun getColor(): Color {
        return Color.getHSBColor(355.0f / 365.0f, 0.52f, 0.88f)
    }
}

class StringToken(content: String, start: Int, end: Int) : BaseToken(content, start, end) {
    override fun getColor(): Color {
        return Color.getHSBColor(95.0f / 365.0f, 0.3778f, 0.7644f)
    }
}

class LiteralToken(content: String, start: Int, end: Int) : BaseToken(content, start, end) {
    override fun getColor(): Color {
        return Color.getHSBColor(355.0f / 365.0f, 0.52f, 0.88f)
    }
}

class IdentifierToken(content: String, start: Int, end: Int) : BaseToken(content, start, end)

private val luaTokenMap = mapOf(
        COMMENT to CommentToken::class.java,
        WHITESPACE to WhiteSpaceToken::class.java,
        STRING to StringToken::class.java,
        KEYWORD to KeywordToken::class.java,
        NUMBER_LITERAL to LiteralToken::class.java,
        IDENTIFIER to IdentifierToken::class.java
)

@Component
@Scope("prototype")
@Qualifier("lexer.lua")
class LuaLexer : BaseLexer() {
    private val lexer = LuaLex(null)

    private lateinit var fullText: String

    override fun lex(input: String) {
        fullText = input
        tokens.clear()

        lexer.reset(input, 0, input.length, LuaLex.YYINITIAL)
        lexer.yylex()

        tokens.addAll(lexer.tokens.mapNotNull {
          luaTokenMap[it.type]?.getConstructor(String::class.java, Int::class.java, Int::class.java)?.newInstance(it.content, it.start, it.end)
        })
    }

    override fun getFullText(): String {
        return fullText
    }
}