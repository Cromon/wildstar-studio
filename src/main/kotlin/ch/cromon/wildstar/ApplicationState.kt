package ch.cromon.wildstar

import ch.cromon.wildstar.utils.Event
import ch.cromon.wildstar.utils.EventHandler
import kotlin.properties.Delegates

enum class ApplicationState {
    FILE_SELECT,
    INITIAL_LOADING,
    LOAD_DONE
}

object StateManager {
    private val stateChangedEvent = EventHandler<ApplicationState>()

    var currentState by Delegates.observable(ApplicationState.FILE_SELECT, { _, _, _ -> stateChanged() })
    var stateChanged = Event(stateChangedEvent)

    private fun stateChanged() {
        stateChangedEvent(currentState)
    }
}