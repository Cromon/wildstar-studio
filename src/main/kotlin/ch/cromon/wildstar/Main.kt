package ch.cromon.wildstar

import ch.cromon.wildstar.ui.Window
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.AnnotationConfigApplicationContext
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.stereotype.Component

@Configuration
@ComponentScan
open class Scanner

@Component
class ClientRunner {
    @Autowired
    private lateinit var window: Window

    fun run() {
        window.show()
        window.runLoop()
    }
}

fun main(args: Array<String>) {
    AnnotationConfigApplicationContext().use {
        it.scan(Scanner::class.java.`package`.name)
        it.refresh()
        it.getBean(ClientRunner::class.java).run()
    }
}