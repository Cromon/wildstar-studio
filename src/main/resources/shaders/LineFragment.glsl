
uniform vec4 lineColor;
uniform float gradientOffset;

varying vec2 texCoord;

void main() {
    float tcDiff = abs(texCoord.y);
    float indicator = step(gradientOffset, tcDiff);
    float alpha = indicator * mix(1.0, 0.0, (tcDiff - gradientOffset) / (1 - gradientOffset)) + (1 - indicator);
    gl_FragColor = vec4(lineColor.rgb, alpha);
}
