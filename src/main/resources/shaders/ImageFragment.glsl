varying vec2 texCoord;

uniform sampler2D imageTexture;

void main() {
    gl_FragColor = texture(imageTexture, texCoord);
}
