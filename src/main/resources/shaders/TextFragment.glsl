#version 130

varying vec2 textureCoord;

uniform sampler2D textTexture;
uniform vec4 color;
uniform vec2 dimension;

float getAlpha(vec2 coords) {
    float stepx = 0.5 / dimension.x;
    float stepy = 0.5 / dimension.y;
    float c1 = texture(textTexture, coords + vec2(0, -stepy)).a;
    float c2 = texture(textTexture, coords + vec2(stepx, 0)).a;
    float c3 = texture(textTexture, coords + vec2(0, stepy)).a;
    float c4 = texture(textTexture, coords + vec2(-stepx, 0)).a;
    float c5 = texture(textTexture, coords).a;
    return (c1 + c2 + c3 + c4 + c5) / 5.0;
}

void main() {
    gl_FragColor = vec4(color.rgb, getAlpha(textureCoord));
}