attribute vec2 position0;
attribute vec2 texCoord0;

uniform mat4 matProjection;
uniform mat4 matTransform;

varying vec2 texCoord;

void main() {
    texCoord = texCoord0;
    gl_Position = matProjection * matTransform * vec4(position0, 0, 1);
}
