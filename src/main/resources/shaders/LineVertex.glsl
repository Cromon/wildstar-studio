attribute vec2 position0;
attribute vec2 texCoord0;

uniform mat4 matProjection;
uniform mat4 globalTransform;

varying vec2 texCoord;

void main() {
    gl_Position = matProjection * globalTransform * vec4(position0, 0.0, 1.0);
    texCoord = texCoord0;
}
